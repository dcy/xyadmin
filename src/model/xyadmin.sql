-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL COMMENT '用户名',
  `password_hash` varchar(60) NOT NULL COMMENT 'hash过的密码',
  `type` int(11) NOT NULL COMMENT '用户类型(0普通，1管理员)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `accounts` (`id`, `name`, `password_hash`, `type`) VALUES
(1,	'root',	'434d492631d0a88a30b7f98da2422957',	1),
(2,	'test',	'9c460cabb997427f487103bce376acd3',	1);

DROP TABLE IF EXISTS `channels`;
CREATE TABLE `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL COMMENT '名字',
  `tag` varchar(60) NOT NULL COMMENT '标签',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL COMMENT '谁发的',
  `type` varchar(16) NOT NULL COMMENT '类型',
  `title` varchar(60) NOT NULL COMMENT '标题',
  `attachments` varchar(3000) NOT NULL COMMENT '附件',
  `svr_ids` varchar(3000) NOT NULL COMMENT '发送的服务器',
  `users` varchar(6000) NOT NULL COMMENT '接收的用户列表',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `gifts`;
CREATE TABLE `gifts` (
  `sn` varchar(60) NOT NULL COMMENT '编号',
  `type` varchar(60) NOT NULL COMMENT '类型',
  `items` varchar(3000) NOT NULL COMMENT '礼品列表',
  `code` varchar(60) NOT NULL COMMENT '固定码',
  `amount` int(11) NOT NULL COMMENT '数量',
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '开始时间',
  `deadline` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '截止时间',
  `email_title` varchar(60) NOT NULL COMMENT '邮件标题',
  `email_content` varchar(3000) NOT NULL COMMENT '邮件内容',
  `remark1` varchar(2000) NOT NULL COMMENT '备注1',
  `remark2` varchar(2000) NOT NULL COMMENT '备注2',
  PRIMARY KEY (`sn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `notices`;
CREATE TABLE `notices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(16) NOT NULL COMMENT '类型，QA，审核，正式',
  `vsn` varchar(16) NOT NULL COMMENT '版本号',
  `platforms` varchar(60) NOT NULL COMMENT '所选平台',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `serverlists`;
CREATE TABLE `serverlists` (
    `svr_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '服务器id',
    `name` varchar(60) NOT NULL COMMENT '服务器名',
    `clt_vsn` varchar(60) NOT NULL COMMENT '客户端版本号',
    `area_name` varchar(60) NOT NULL COMMENT '大区名称',
    `state` int(11) NOT NULL COMMENT '状态',
    `type` int(11) NOT NULL COMMENT '类型',
    `ip` varchar(60) NOT NULL COMMENT '游戏服务器地址',
    `local_ip` varchar(60) NOT NULL COMMENT '内网地址',
    `port` int(11) NOT NULL COMMENT '游戏服务器端口',
    `gm_port` int(11) NOT NULL COMMENT 'gm服务端口',
    `log_port` int(11) NOT NULL COMMENT '日志端口',
    `platforms` varchar(60) NOT NULL COMMENT '所属平台',
    `channels` varchar(2000) NOT NULL COMMENT '所属渠道',
    `clt_update_url` varchar(2000) NOT NULL COMMENT '客户端整包更新地址',
    `activity_list_id` int(11) NOT NULL COMMENT '活动列表',
    `locked_funcs` varchar(1000) NOT NULL DEFAULT '[]' COMMENT '锁住的功能id列表',
    `gift_sns` varchar(1000) NOT NULL DEFAULT '[]' COMMENT '礼包码列表',
    `activity_infos` text NOT NULL COMMENT '活动列表',
    PRIMARY KEY (`svr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sysmsgs`;
CREATE TABLE `sysmsgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_type` int(11) NOT NULL COMMENT '消息类型，0滚动，1普通',
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '开始时间',
  `times` int(11) NOT NULL COMMENT '次数',
  `interval_minute` int(11) NOT NULL COMMENT '间隔分钟数',
  `content` varchar(1000) NOT NULL COMMENT '内容',
  `svr_ids` varchar(8000) NOT NULL COMMENT '服务器id列表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `whitelists`;
CREATE TABLE `whitelists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(60) NOT NULL COMMENT '帐号id',
  `username` varchar(60) NOT NULL COMMENT '用户名',
  `is_advanced` tinyint(1) NOT NULL COMMENT '是否高级帐号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `udid_whitelists`;
CREATE TABLE `udid_whitelists` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `udid` varchar(160) NOT NULL,
    `remark` varchar(160) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `udid` (`udid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `notifys`;
CREATE TABLE `notifys` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `sender_id` int(11) NOT NULL COMMENT '谁发的',
    `title` varchar(60) NOT NULL COMMENT '标题',
    `content` varchar(1000) NOT NULL COMMENT '内容',
    `is_and` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0或1与',
    `svr_ids` varchar(6000) NOT NULL DEFAULT '[]' COMMENT '所选服务器',
    `is_pay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否添加充值条件',
    `level_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '等级类型',
    `level_value` int(11) NOT NULL DEFAULT '0' COMMENT '等级值',
    `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- 2015-07-27 03:40:33
