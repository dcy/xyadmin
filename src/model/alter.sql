-- 2015-07-27 11:36
ALTER TABLE `serverlists`
ADD `gift_sns` varchar(1000) COLLATE 'utf8_general_ci' NOT NULL DEFAULT '[]' COMMENT '礼包码列表';

DROP TABLE IF EXISTS `gifts`;
CREATE TABLE `gifts` (
  `sn` varchar(60) NOT NULL COMMENT '编号',
  `type` varchar(60) NOT NULL COMMENT '类型',
  `items` varchar(3000) NOT NULL COMMENT '礼品列表',
  `code` varchar(60) NOT NULL COMMENT '固定码',
  `amount` int(11) NOT NULL COMMENT '数量',
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '开始时间',
  `deadline` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '截止时间',
  `email_title` varchar(60) NOT NULL COMMENT '邮件标题',
  `email_content` varchar(3000) NOT NULL COMMENT '邮件内容',
  `remark1` varchar(2000) NOT NULL COMMENT '备注1',
  `remark2` varchar(2000) NOT NULL COMMENT '备注2',
  PRIMARY KEY (`sn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- end


-- 2015-08-13 20:58
ALTER TABLE `serverlists`
CHANGE `channels` `channels` varchar(2000) COLLATE 'utf8_general_ci' NOT NULL COMMENT '所属渠道' AFTER `platforms`,
ADD `activity_infos` text COLLATE 'utf8_general_ci' NOT NULL COMMENT '活动列表';
-- end

-- 2015-10-07 10:56
DROP TABLE IF EXISTS `udid_whitelists`;
CREATE TABLE `udid_whitelists` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `udid` varchar(160) NOT NULL,
    `remark` varchar(160) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `udid` (`udid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- end

-- notify
-- 2015-12-21 20:58
DROP TABLE IF EXISTS `notifys`;
CREATE TABLE `notifys` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `sender_id` int(11) NOT NULL COMMENT '谁发的',
    `title` varchar(60) NOT NULL COMMENT '标题',
    `content` varchar(1000) NOT NULL COMMENT '内容',
    `is_and` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0或1与',
    `svr_ids` varchar(6000) NOT NULL DEFAULT '[]' COMMENT '所选服务器',
    `is_pay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否添加充值条件',
    `level_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '等级类型',
    `level_value` int(11) NOT NULL DEFAULT '0' COMMENT '等级值',
    `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- end
