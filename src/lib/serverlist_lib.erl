-module(serverlist_lib).
-compile(export_all).
-include("xyadmin.hrl").

get_serverlists() ->
    AllServerlists = ets:tab2list(serverlists),
    [Serverlist || {_, Serverlist} <- AllServerlists].

get_sorted_serverlists() ->
    Serverlists = get_serverlists(),
    Fun = fun(FServerlist, BServerlist) ->
                  maps:get(svr_id, FServerlist) =< maps:get(svr_id, BServerlist)
          end,
    lists:sort(Fun, Serverlists).

format_serverlist(Serverlist) when erlang:is_map(Serverlist) ->
    #{type:=TypeId, state:=StateId, channels:=ChannelIds, platforms:=PlatformIds,
      locked_funcs:=LockedFuncIds, gift_sns:=GiftSns, activity_infos:=ActivityInfos} = Serverlist,
    Type = util:get_svr_type(TypeId),
    State = util:get_svr_state(StateId),
    ChannelFun = fun(ChannelId) ->
                         Channel = util:get_ets(channels, ChannelId),
                         maps:to_list(Channel)
                 end,
    Channels = lists:map(ChannelFun, ChannelIds),
    PlatformFun = fun(PlatformId) ->
                          Platform = util:get_platform(PlatformId),
                          maps:to_list(Platform)
                  end,
    Platforms = lists:map(PlatformFun, PlatformIds),
    FuncFun = fun(FuncId) ->
                      [{func_id, FuncId}]
              end,
    LockedFuncs = lists:map(FuncFun, LockedFuncIds),
    NewServerlist = Serverlist#{type=>maps:to_list(Type), state=>maps:to_list(State),
                                platforms=>Platforms, channels=>Channels,
                                locked_funcs=>LockedFuncs,
                                gift_sns=>[list_to_binary(GiftSn) || GiftSn <- GiftSns],
                                activity_infos=>[format_activity(Info) || Info <- ActivityInfos] 
                               },
    maps:to_list(NewServerlist).

format_activity(ActivityInfo) ->
    #{id:=Id} = ActivityInfo,
    Activity = activity_lib:get_activity(Id),
    #{title:=Title, content:=Content} = Activity,
    NewActivityInfo = ActivityInfo#{title=>Title, content=>Content},
    maps:to_list(NewActivityInfo).


get_format_serverlists() ->
    SortLists = get_sorted_serverlists(),
    lists:map(fun format_serverlist/1, SortLists).

get_simple_serverlists() ->
    Serverlists = serverlist_lib:get_serverlists(),
    OrderFun = fun(Flist, Blist) ->
                       maps:get(svr_id, Flist) =< maps:get(svr_id, Blist)
               end,
    SortLists = lists:sort(OrderFun, Serverlists),
    Fun = fun(Serverlist) ->
                  #{svr_id:=SvrId, name:=Name} = Serverlist,
                  [{svr_id, SvrId}, {name, Name}]
          end,
    lists:map(Fun, SortLists).

get_serverlist(SvrId) ->
    util:get_ets(serverlists, SvrId).


save_serverlists_files(PlatformIds, ChannelIds) ->
    spawn(fun() -> do_save_serverlists_files(PlatformIds, ChannelIds) end).

do_save_serverlists_files(PlatformIds, ChannelIds) ->
    [save_serverlists_file(PlatformId, ChannelId) || PlatformId <- PlatformIds, ChannelId <- ChannelIds].

save_serverlists_file(PlatformId, ChannelId) ->
    Platform = util:get_platform(PlatformId),
    #{tag:=PlatformTag} = Platform,
    Channel = channel_lib:get_channel(ChannelId),
    ChannelTag = util:to_list(maps:get(tag, Channel)),
    Serverlists = get_serverlists(PlatformId, ChannelId),
    TempFile = lists:concat([PlatformTag, "_", ChannelTag, "_serverlists"]),
    {ok, File} = file:open(TempFile, [write]),
    [save_serverlist_file(Serverlist, File) || Serverlist <- Serverlists],
    file:close(File),
    FilesPath = util:get_system_config(files_path),
    DestFile = lists:concat([FilesPath, PlatformTag, "_", ChannelTag, "_serverlists"]),
    file:rename(TempFile, DestFile),
    ok.

save_serverlist_file(Serverlist, File) ->
    #{svr_id:=SvrId, name:=Name, clt_vsn:=CltVsn, area_name:=AreaName, state:=State, type:=Type,
      ip:=Ip, port:=Port, log_port:=LogPort, clt_update_url:=CltUrl,
      activity_list_id:=ActivityListId} = Serverlist,
    CltUpdateUrl = case CltUrl =:= "" orelse CltUrl =:= <<>> of
                       true -> "*";
                       false -> CltUrl
                   end,
    Str = io_lib:fwrite("~p ~s ~s ~s ~p ~p ~s ~p ~p ~s ~p\n", [SvrId, Name, CltVsn, AreaName, State, Type, Ip, Port, LogPort, CltUpdateUrl, ActivityListId]),
    file:write(File, Str).


get_serverlists(PlatformId, ChannelId) ->
    Serverlists = get_sorted_serverlists(),
    Fun = fun(Serverlist) ->
                  #{platforms:=PlatformIds, channels:=ChannelIds} = Serverlist,
                  lists:member(PlatformId, PlatformIds) andalso  lists:member(ChannelId, ChannelIds)
          end,
    lists:filter(Fun, Serverlists).

has_channel(ChannelId) ->
    Serverlists = get_serverlists(),
    has_channel(ChannelId, Serverlists).
has_channel(_ChannelId, []) ->
    undefined;
has_channel(ChannelId, [Serverlist | Serverlists]) ->
    #{channels:=ChannelIds} = Serverlist,
    case lists:member(ChannelId, ChannelIds) of
        true -> Serverlist;
        false -> has_channel(ChannelId, Serverlists)
    end.

get_simple_info(SvrId) ->
    Serverlist = serverlist_lib:get_serverlist(SvrId),
    #{name:=Name} = Serverlist,
    [{svr_id, SvrId}, {name, Name}].

add_gift_sn(SvrId, GiftSn) ->
    Serverlist = get_serverlist(SvrId),
    #{gift_sns:=OriGiftSns} = Serverlist,
    GiftSns = sets:to_list(sets:from_list([GiftSn | OriGiftSns])),
    NewServerlist = Serverlist#{gift_sns => GiftSns},
    db:execute(<<"UPDATE serverlists SET gift_sns=? WHERE svr_id=?">>,
               [util:term_to_bitstring(GiftSns), SvrId]),
    util:put_ets(serverlists, SvrId, NewServerlist),
    ok.

update_activity_info(SvrId, ActivityInfo) ->
    Serverlist = get_serverlist(SvrId),
    #{activity_infos:=ActivityInfos} = Serverlist,
    NewActivityInfos = ActivityInfos ++ [ActivityInfo],
    NewServerlist = Serverlist#{activity_infos => NewActivityInfos},
    db:execute(<<"UPDATE serverlists SET activity_infos=? WHERE svr_id=?">>,
               [util:term_to_bitstring(NewActivityInfos), SvrId]),
    util:put_ets(serverlists, SvrId, NewServerlist),
    ok.

cancel_activity(SvrId, ActivityId) ->
    Serverlist = get_serverlist(SvrId),
    #{activity_infos:=OriActivityInfos} = Serverlist,
    Fun = fun(ActivityInfo, Infos) ->
                  case maps:get(id, ActivityInfo) of
                      ActivityId -> Infos;
                      _ -> Infos ++ [ActivityInfo]
                  end
          end,
    ActivityInfos = lists:foldl(Fun, [], OriActivityInfos),
    NewServerlist = Serverlist#{activity_infos => ActivityInfos},
    db:execute(<<"UPDATE serverlists SET activity_infos=? WHERE svr_id=?">>,
               [util:term_to_bitstring(ActivityInfos), SvrId]),
    util:put_ets(serverlists, SvrId, NewServerlist),
    ok.


