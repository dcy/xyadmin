-module(db).
-compile(export_all).
-include("xyadmin.hrl").

execute(Sql) when is_list(Sql) orelse is_binary(Sql) ->
    case emysql:execute(?DB_POOL, Sql) of
        {result_packet, _, _, Rows, _} -> Rows;
        {ok_packet, _, _, UpdatedId, _, _, _} -> UpdatedId;
        {error_packet, _, _, _, Msg} -> erlang:error({mysqlDbError, {Sql, io_lib:format("~s", [Msg])}});
        _Other -> ok
    end.

async_execute(Sql) ->
    spawn(fun() -> execute(Sql) end).

execute(Sql, Args) when (is_list(Sql) orelse is_binary(Sql)) andalso is_list(Args) ->
    case emysql:execute(?DB_POOL, Sql, Args) of
        {result_packet, _, _, Rows, _} -> Rows;
        {ok_packet, _, _, UpdatedId, _, _, _} -> UpdatedId;
        {error_packet, _, _, _, Msg} -> erlang:error({mysqlDbError, {Sql, io_lib:format("~s", [Msg])}});
        _Other -> ok
    end.

async_execute(Sql, Args) ->
    spawn(fun() -> execute(Sql, Args) end).

prepared_execute(Sql, Args) when (is_list(Sql) orelse is_binary(Sql)) andalso is_list(Args) ->
    StmtName = erlang:list_to_atom(util:md5(Sql)),
    emysql:prepare(StmtName, Sql),
    case emysql:execute(?DB_POOL, StmtName, Args) of
        {result_packet, _, _, Rows, _} -> Rows;
        {ok_packet, _, _, UpdatedId, _, _, _} -> UpdatedId;
        {error_packet, _, _, _, Msg} -> erlang:error({mysqlDbError, {Sql, Args, io_lib:format("~s", [Msg])}});
        _Other -> ok
    end.

async_prepared_execute(Sql, Args) ->
    spawn(fun() -> prepared_execute(Sql, Args) end).

select_as_maps(SqlBin) ->
    Result = emysql:execute(?DB_POOL, SqlBin),
    emysql:as_maps(Result).

select_as_maps(SqlStmt, Args) ->
    Result = emysql:execute(?DB_POOL, SqlStmt, Args),
    emysql:as_maps(Result).

prepared_select_as_maps(Sql, Args) when (is_list(Sql) orelse is_binary(Sql)) andalso is_list(Args) ->
    StmtName = erlang:list_to_atom(util:md5(Sql)),
    emysql:prepare(StmtName, Sql),
    Result = emysql:execute(?DB_POOL, StmtName, Args),
    emysql:as_maps(Result).

select_as_proplist(SqlBin) ->
    Result = emysql:execute(?DB_POOL, SqlBin),
    emysql:as_proplist(Result).

select_as_proplist(SqlStmt, Args) ->
    Result = emysql:execute(?DB_POOL, SqlStmt, Args),
    emysql:as_proplist(Result).
