-module(channel_lib).
-compile(export_all).
-include("xyadmin.hrl").

get_channels() ->
    AllChannels = ets:tab2list(channels),
    [Channel || {_, Channel} <- AllChannels].

get_format_channels() ->
    Channels = ets:tab2list(channels),
    [maps:to_list(Channel) || {_, Channel} <- Channels].

get_channel(ChannelId) ->
    util:get_ets(channels, ChannelId).
