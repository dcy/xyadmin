-module(activity_lib).
-compile(export_all).
-include("xyadmin.hrl").

gen_activities() ->
    FileName = "datas/Activity.csv",
    {ok, Data} = file:read_file(FileName),
    ActivitiesBin = binary:split(Data, [<<"\n">>], [global]),
    Fun = fun(ActivityBin) ->
                  [IdBin, ContentBin, TitleBin] = binary:split(ActivityBin, [<<",">>], [global]),
                  Id = binary_to_integer(IdBin),
                  Activity = #{id=>Id, title=>TitleBin, content=>ContentBin},
                  util:put_ets(activities, Id, Activity),
                  Activity
          end,
    [Fun(Activity) || Activity <- ActivitiesBin, Activity =/= <<>>].

get_activity(Id) ->
    util:get_ets(activities, Id).

send_svrs(SvrIds, ActivityInfo) ->
    Serverlists = [serverlist_lib:get_serverlist(SvrId) || SvrId <- SvrIds],
    do_send_svrs(Serverlists, ActivityInfo).

send_svrs(ActivityInfo) ->
    Serverlists = serverlist_lib:get_serverlists(),
    do_send_svrs(Serverlists, ActivityInfo).

do_send_svrs(Serverlists, ActivityInfo) ->
    Fun = fun(Serverlist, Svrs) ->
                  case xyadmin_clt:send_activity_svr(Serverlist, ActivityInfo) of
                      ok ->
                          serverlist_lib:update_activity_info(maps:get(svr_id, Serverlist), ActivityInfo),
                          Svrs;
                      not_ok ->
                          #{svr_id:=SvrId, name:=Name} = Serverlist,
                          Info = [{svr_id, SvrId}, {name, Name}],
                          [Info | Svrs]
                  end
          end,
    lists:foldl(Fun, [], Serverlists).

cancel_svrs(SvrIds, ActivityId) ->
    Serverlists = [serverlist_lib:get_serverlist(SvrId) || SvrId <- SvrIds],
    do_cancel_svrs(Serverlists, ActivityId).

cancel_svrs(ActivityId) ->
    Serverlists = serverlist_lib:get_serverlists(),
    do_cancel_svrs(Serverlists, ActivityId).

do_cancel_svrs(Serverlists, ActivityId) ->
    Fun = fun(Serverlist, Svrs) ->
                  case xyadmin_clt:cancel_activity(Serverlist, ActivityId) of
                      ok ->
                          serverlist_lib:cancel_activity(maps:get(svr_id, Serverlist), ActivityId),
                          Svrs;
                      not_ok ->
                          #{svr_id:=SvrId, name:=Name} = Serverlist,
                          Info = [{svr_id, SvrId}, {name, Name}],
                          [Info | Svrs]
                  end
          end,
    lists:foldl(Fun, [], Serverlists).
