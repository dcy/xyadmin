-module(whitelist_lib).
-compile(export_all).
-include("xyadmin.hrl").

get_sorted_whitelists() ->
    Whitelists = [Whitelist || {_, Whitelist} <- ets:tab2list(whitelists)],
    Fun = fun(FWhitelist, BWhitelist) ->
                  maps:get(id, FWhitelist) =< maps:get(id, BWhitelist)
          end,
    lists:sort(Fun, Whitelists).


get_sorted_udid_whitelists() ->
    Whitelists = [Whitelist || {_, Whitelist} <- ets:tab2list(udid_whitelists)],
    Fun = fun(FWhitelist, BWhitelist) ->
                  maps:get(id, FWhitelist) =< maps:get(id, BWhitelist)
          end,
    lists:sort(Fun, Whitelists).


save_whitelists_file() ->
    spawn(fun() -> do_save_whitelists_file() end).

do_save_whitelists_file() ->
    Whitelists = get_sorted_whitelists(),
    %FileName = lists:concat([TempPath, "whitelists"]),
    FileName = "whitelists",
    {ok, File} = file:open(FileName, [write]),
    Fun = fun(Whitelist) ->
                  #{account_id:=AccountId, is_advanced:=IsAdvanced} = Whitelist,
                  Str = io_lib:fwrite("~s ~p\n", [util:to_list(AccountId), IsAdvanced]),
                  %file:write_file(File, Str, [append])
                  file:write(File, Str)
          end,
    lists:foreach(Fun, Whitelists),
    file:close(File),
    FilesPath = util:get_system_config(files_path),
    DestFile = lists:concat([FilesPath, "whitelists"]),
    file:rename(FileName, DestFile),
    ok.

save_udid_whitelists_file() ->
    spawn(fun() -> do_save_udid_whitelists_file() end).

do_save_udid_whitelists_file() ->
    Whitelists = get_sorted_udid_whitelists(),
    FileName = "udid_whitelists",
    {ok, File} = file:open(FileName, [write]),
    Fun = fun(Whitelist) ->
                  #{udid:=Udid} = Whitelist,
                  Str = io_lib:fwrite("~s\n", [Udid]),
                  file:write(File, Str)
          end,
    lists:foreach(Fun, Whitelists),
    file:close(File),
    FilesPath = util:get_system_config(files_path),
    DestFile = lists:concat([FilesPath, "udid_whitelists"]),
    file:rename(FileName, DestFile),
    ok.


