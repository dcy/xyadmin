-module(gift_lib).
-compile(export_all).
-include("xyadmin.hrl").

get_gifts(Type) ->
    AllGifts = [Gift || {_, Gift} <- ets:tab2list(gifts)],
    FilterFun = fun(Gift) ->
                        maps:get(type, Gift) =:= Type
                end,
    lists:filter(FilterFun, AllGifts).

gen_gift_codes(Type, Gift) ->
    #{sn:=Sn, amount:=Amount} = Gift,
    FileName = lists:concat(["priv/static/gifts/", Sn, "_", Type, "_gift_code"]),
    {ok, File} = file:open(FileName, [write]),
    case Type of
        "unique" ->
            gen_gift_code(File, Sn, 1, 0, Amount);
        "fixed" ->
            gen_fixed_gift_code(File, Gift)
    end,
    file:close(File),
    ok.

gen_fixed_gift_code(File, Gift) ->
    file:write(File, maps:get(code, Gift)).

gen_gift_code(_File, _Sn, Index, GenAmount, TotalAmount) when GenAmount >= TotalAmount ->
    ?TRACE_VAR({Index, GenAmount}),
    ets:delete_all_objects(gen_gifts),
    ok;
gen_gift_code(File, Sn, Index, GenAmount, TotalAmount) ->
    SnIndex = lists:concat([Sn, Index]),
    Md5 = mochihex:to_hex(erlang:md5(SnIndex)),
    Md5List = lists:sublist(Md5, 10),
    case util:get_ets(gen_gifts, Md5List) of
        undefined ->
            Str = io_lib:fwrite("~s\n", [Sn ++ Md5List]),
            file:write(File, Str),
            gen_gift_code(File, Sn, Index+1, GenAmount+1, TotalAmount),
            util:put_ets(gen_gifts, Md5List, true);
        _ ->
            gen_gift_code(File, Sn, Index+1, GenAmount, TotalAmount)
    end.

refresh_erlang_files() ->
    Gifts = [Gift || {_, Gift} <- ets:tab2list(gifts)],
    Now = util:unixtime(),
    GiftTypeFileName = lists:concat(["priv/static/gifts/", "gift_type.erl"]),
    {ok, GiftTypeFile} = file:open(GiftTypeFileName, [write]),
    TypeHead = io_lib:fwrite("~s\n", ["-module(gift_type).\n-export([get/1]).\n"]),
    file:write(GiftTypeFile, TypeHead),
    GiftCodeFileName = lists:concat(["priv/static/gifts/", "gift_code.erl"]),
    {ok, GiftCodeFile} = file:open(GiftCodeFileName, [write]),
    CodeHead = io_lib:fwrite("~s\n", ["-module(gift_code).\n-export([get/1]).\n"]),
    file:write(GiftCodeFile, CodeHead),
    Fun = fun(Gift) ->
                  #{sn:=Sn, type:=Type, deadline:=Deadline} = Gift,
                  case Now =< util:strtime_to_unixtime(Deadline) of
                      true ->
                          FormatedGift = maps:remove(remark2, maps:remove(remark1, Gift)),
                          Str = io_lib:fwrite("get(~s) ->\n    ~p;~n", [Sn, FormatedGift]),
                          file:write(GiftTypeFile, Str),
                          refresh_gift_code_erl(GiftCodeFile, Sn, Type),
                          ok;
                      false ->
                          do_nothing
                  end
          end,
    lists:foreach(Fun, Gifts),
    TypeTail = io_lib:fwrite("~s\n", ["get(_) ->\n    undefined."]),
    file:write(GiftTypeFile, TypeTail),
    CodeTail = io_lib:fwrite("~s\n", ["get(_) ->\n    undefined."]),
    file:write(GiftCodeFile, CodeTail),
    
    ok.

refresh_gift_code_erl(File, Sn, Type) ->
    FileName = lists:concat(["priv/static/gifts/", Sn, "_", Type, "_gift_code"]),
    {ok, Data} = file:read_file(FileName),
    CodesBin = binary:split(Data, [<<"\n">>], [global]),
    Fun = fun(CodeBin) ->
                  case CodeBin of
                      <<>> ->
                          do_nothing;
                      _ ->
                          Code = binary_to_list(CodeBin),
                          Str = io_lib:fwrite("get(~p) ->\n    ~s;~n", [Code, Sn]),
                          file:write(File, Str)
                  end
          end,
    lists:foreach(Fun, CodesBin),
    ok.

is_code_exist(Type, Code) ->
    case Type of
        "fixed" ->
            Gifts = [Gift || {_, Gift} <- ets:tab2list(gifts)],
            is_code_exist1(Gifts, Code);
        "unique" ->
            false
    end.

is_code_exist1([], _Code) ->
    false;
is_code_exist1([Gift|Gifts], Code) ->
    case maps:get(type, Gift) == "fixed" andalso maps:get(code, Gift) == Code of
        true -> true;
        false -> is_code_exist1(Gifts, Code)
    end.

format_gift(Gift) ->
    #{email_title:=OriEmailTitle, email_content:=OriEmailContent} = Gift,
    EmailTitle = case is_binary(OriEmailTitle) of
                     true -> OriEmailTitle;
                     false -> list_to_binary(OriEmailTitle)
                 end,
    EmailContent = case is_binary(OriEmailContent) of
                       true -> OriEmailContent;
                       false -> list_to_binary(OriEmailContent)
                   end,
    Gift#{email_title=>EmailTitle, email_content=>EmailContent}.

send_gift_svrs(Gift, CodeData) ->
    Serverlists = serverlist_lib:get_serverlists(),
    do_send_gift_svrs(Serverlists, Gift, CodeData).

send_gift_svrs(SvrIds, Gift, CodeData) ->
    Serverlists = [serverlist_lib:get_serverlist(SvrId) || SvrId <- SvrIds],
    do_send_gift_svrs(Serverlists, Gift, CodeData).

do_send_gift_svrs(Serverlists, Gift, CodeData) ->
    Payload = jsx:encode(#{action=>"handle_gift", gift=>format_gift(Gift), code_data=>CodeData}),
    Fun = fun(Serverlist, Svrs) ->
                  case xyadmin_clt:send_gift_svr(Serverlist, Payload) of
                      ok ->
                          serverlist_lib:add_gift_sn(maps:get(svr_id, Serverlist), maps:get(sn, Gift)),
                          Svrs;
                      not_ok ->
                          #{svr_id:=SvrId, name:=Name} = Serverlist,
                          Info = [{svr_id, SvrId}, {name, Name}],
                          [Info | Svrs]
                  end
          end,
    lists:foldl(Fun, [], Serverlists).

