-module(lock_lib).
-compile(export_all).
-include("xyadmin.hrl").

lock_funcs(SvrId, LockedFuncs) ->
    Serverlist = serverlist_lib:get_serverlist(SvrId),
    case xyadmin_clt:lock_funcs(SvrId, LockedFuncs) of
        ok ->
            db:async_execute(<<"UPDATE serverlists SET locked_funcs=? WHERE svr_id=?">>, [util:term_to_bitstring(LockedFuncs), SvrId]),
            NewList = Serverlist#{locked_funcs=>LockedFuncs},
            util:put_ets(serverlists, SvrId, NewList),
            ok;
        not_ok ->
            not_ok
    end.
    
