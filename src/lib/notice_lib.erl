-module(notice_lib).
-compile(export_all).
-include("xyadmin.hrl").


get_notices(Type) ->
    AllNotices = [Notice || {_, Notice} <- ets:tab2list(notices)],
    FilterFun = fun(Notice) ->
                        maps:get(type, Notice) =:= Type
                end,
    Notices = lists:filter(FilterFun, AllNotices),
    SortFun = fun(FNotice, BNotice) ->
                      maps:get(id, FNotice) >= maps:get(id, BNotice)
              end,
    lists:sort(SortFun, Notices).

save_notices_files() ->
    spawn(fun() -> do_save_notices_files() end).

do_save_notices_files() ->
    lists:foreach(fun save_type_notices_files/1, ?SVR_TYPES).

ensure_open_files(Type) ->
    Fun = fun(Platform) ->
                  FileName = lists:concat([maps:get(tag, Type), "_", maps:get(tag, Platform), "_notices"]),
                  {ok, File} = file:open(FileName, [write]),
                  put({file, Type, Platform}, File)
          end,
    lists:foreach(Fun, ?PLATFORMS).

handle_end_write_files(Type) ->
    FilesPath = util:get_system_config(files_path),
    Fun = fun(Platform) ->
                  FileName = lists:concat([maps:get(tag, Type), "_", maps:get(tag, Platform), "_notices"]),
                  DestFile = lists:concat([FilesPath, maps:get(tag, Type), "_", maps:get(tag, Platform), "_notices"]),
                  file:rename(FileName, DestFile),
                  File = get({file, Type, Platform}),
                  file:close(File)
          end,
    lists:foreach(Fun, ?PLATFORMS).


save_type_notices_files(Type) ->
    ensure_open_files(Type),
    Notices = get_notices(maps:get(tag, Type)),
    Fun = fun(Notice) ->
                  #{vsn:=Vsn, platforms:=PlatformIds, content:=Content} = Notice,
                  PFFun = fun(PlatformId) ->
                                  Platform = util:get_platform(PlatformId),
                                  File = get({file, Type, Platform}),
                                  NewContent = re:replace(Content, "\n", "\\\\n", [global, {return, list}]),
                                  Str = io_lib:fwrite("~s ~s\n", [Vsn, NewContent]),
                                  file:write(File, Str)
                          end,
                  lists:foreach(PFFun, PlatformIds)
          end,
    lists:foreach(Fun, Notices),
    handle_end_write_files(Type).

