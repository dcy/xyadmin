-module(xyadmin_app).

-behaviour(application).

%% Application callbacks
-export([start/2,
         stop/1]).

-export([start/0]).
-include("xyadmin.hrl").
-include("deps/mysql/src/mysql.hrl").

%%%===================================================================
%%% Application callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application is started using
%% application:start/[1,2], and should start the processes of the
%% application. If the application is structured according to the OTP
%% design principles as a supervision tree, this means starting the
%% top supervisor of the tree.
%%
%% @spec start(StartType, StartArgs) -> {ok, Pid} |
%%                                      {ok, Pid, State} |
%%                                      {error, Reason}
%%      StartType = normal | {takeover, Node} | {failover, Node}
%%      StartArgs = term()
%% @end
%%--------------------------------------------------------------------
start(_StartType, _StartArgs) ->
    'TopSupervisor':start_link().

start() ->
    {ok, _Sup} = xyadmin_sup:start_link(),
    application:ensure_all_started(hackney),
    gen_ets(),
    %start_kernel(),
    start_mysql(),
    %start_cache_sup(),
    load_something(),
    spawn(fun() -> monitor_stop() end),
    {ok, _Sup}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application has stopped. It
%% is intended to be the opposite of Module:start/2 and should do
%% any necessary cleaning up. The return value is ignored.
%%
%% @spec stop(State) -> void()
%% @end
%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================

gen_ets() ->
    ets:new(accounts, [named_table, public, set, {read_concurrency, true}]),
    ets:new(channels, [named_table, public, set, {read_concurrency, true}]),
    ets:new(serverlists, [named_table, public, set, {read_concurrency, true}]),
    ets:new(sysmsgs, [named_table, public, set, {read_concurrency, true}]),
    ets:new(whitelists, [named_table, public, set, {read_concurrency, true}]),
    ets:new(udid_whitelists, [named_table, public, set, {read_concurrency, true}]),
    ets:new(notices, [named_table, public, set, {read_concurrency, true}]),
    ets:new(emails, [named_table, public, set, {read_concurrency, true}]),
    ets:new(gifts, [named_table, public, set, {read_concurrency, true}]),
    ets:new(gen_gifts, [named_table, public, set]),

    ets:new(activities, [named_table, public, set]),
    activity_lib:gen_activities(),

    ets:new(notifys, [named_table, public, set]),
    ok.

start_cache_sup() ->
    {ok, _} = supervisor:start_child(
            yxq_sup,
            {yxq_cache_sup,
             {yxq_cache_sup, start_link, []},
             transient, infinity, supervisor, [yxq_cache_sup]}),
    start_cache_procs(),
    ok.

start_cache_procs() ->
    start_cache_procs(0, ?CACHE_PROCESS_AMOUNT).

start_cache_procs(Max, Max) ->
    ok;
start_cache_procs(Acc, Max) ->
    {ok, _Pid} = supervisor:start_child(yxq_cache_sup, [Acc]),
    start_cache_procs(Acc+1, Max).

start_kernel() ->
    {ok,_} = supervisor:start_child(
               yxq_sup,
               {yxq_kernel,
                {yxq_kernel, start_link,[]},
                permanent, 10000, supervisor, [yxq_kernel]}),
    ok.


monitor_stop() ->
    Ref = erlang:monitor(process, boss_web),
    receive
        {'DOWN', _, process, {boss_web, _}, _} ->
            server_stop()
    end.

handle_msg() ->
    receive
        Msg ->
            handle_msg()
    end.

server_stop() ->
    ?TRACE_VAR(server_stop).

start_mysql() ->
    %DbName = util:get_conf(yxq, path),
    DbName = boss_env:get_env(emysql_db_name, db_name),
    DbHost = boss_env:get_env(emysql_db_host, db_host),
    DbPort = boss_env:get_env(emysql_db_port, db_port),
    DbUser = boss_env:get_env(emysql_db_user, db_user),
    DbPasswd = boss_env:get_env(emysql_db_password, db_passwd),
    
    crypto:start(),
    application:start(emysql),
    emysql:add_pool(?DB_POOL, 100, DbUser, DbPasswd, DbHost, DbPort, DbName, utf8),
    ok.

load_something() ->
    AccountObjs = db:select_as_maps(<<"SELECT * FROM accounts">>),
    [util:put_ets(accounts, maps:get(id, Account), Account) || Account <- AccountObjs],

    ChannelObjs = db:select_as_maps(<<"SELECT * FROM channels">>),
    [util:put_ets(channels, maps:get(id, Channel), Channel) || Channel <- ChannelObjs],

    ServerObjs = db:select_as_maps(<<"SELECT * FROM serverlists">>),
    ServerFun = fun(Server) ->
                        #{platforms:=PlatformsBin, channels:=ChannelsBin,
                          locked_funcs:=LockedFuncsBin, gift_sns:=GiftSnsBin,
                          activity_infos:=OriActivityInfos
                         } = Server,
                        ActivityInfos = case OriActivityInfos of
                                            undefined -> [];
                                            <<>> -> [];
                                            Other -> util:bitstring_to_term(Other)
                                        end,
                        Server#{platforms=>util:bitstring_to_term(PlatformsBin),
                                locked_funcs=>util:bitstring_to_term(LockedFuncsBin),
                                gift_sns=>util:bitstring_to_term(GiftSnsBin),
                                activity_infos=>ActivityInfos,
                                channels=>util:bitstring_to_term(ChannelsBin)}
                end,
    [util:put_ets(serverlists, maps:get(svr_id, Server), ServerFun(Server)) || Server <- ServerObjs],

    SysmsgObjs = db:select_as_maps(<<"SELECT * FROM sysmsgs">>),
    SysmsgFun = fun(Sysmsg) ->
                        #{id:=Id, svr_ids:=SvrIdsBin, start_time:={datetime, StartTime}} = Sysmsg,
                        SvrIds = util:bitstring_to_term(SvrIdsBin),
                        StrTime = util:to_str_time(StartTime),
                        NewSysMsg = Sysmsg#{svr_ids=>SvrIds, start_time=>StrTime},
                        util:put_ets(sysmsgs, Id, NewSysMsg)
                end,
    lists:foreach(SysmsgFun, SysmsgObjs),

    Whitelists = db:select_as_maps(<<"SELECT * FROM whitelists">>),
    [util:put_ets(whitelists, maps:get(id, Whitelist), Whitelist) || Whitelist <- Whitelists],

    UdidWhitelists = db:select_as_maps(<<"SELECT * FROM udid_whitelists">>),
    [util:put_ets(udid_whitelists, maps:get(id, Whitelist), Whitelist) || Whitelist <- UdidWhitelists],

    NoticeObjs = db:select_as_maps(<<"SELECT * FROM notices">>),
    NoticeFun = fun(Notice) ->
                        #{id:=Id, type:=TypeBin, platforms:=PlatformsBin} = Notice,
                        NewNotice = Notice#{platforms=>util:bitstring_to_term(PlatformsBin),
                                            type=>binary_to_list(TypeBin)},
                        util:put_ets(notices, Id, NewNotice)
                end,
    lists:foreach(NoticeFun, NoticeObjs),



    EmailObjs = db:select_as_maps(<<"SELECT * FROM emails">>),
    EmailFun = fun(Email) ->
                       #{id:=Id, type:=TypeBin, attachments:=AttachmentsBin,
                         svr_ids:=SvrIdsBin, users:=UsersBin} = Email,
                       NewEmail = Email#{type=>binary_to_list(TypeBin),
                                         attachments=>util:bitstring_to_term(AttachmentsBin),
                                         svr_ids =>util:bitstring_to_term(SvrIdsBin),
                                         users =>util:bitstring_to_term(UsersBin)
                                        },
                       util:put_ets(emails, Id, NewEmail)
               end,
    lists:foreach(EmailFun, EmailObjs),

    GiftObjs = db:select_as_maps(<<"SELECT * FROM gifts">>),
    GiftFun = fun(Gift) ->
                      #{sn:=SnBin, type:=TypeBin, items:=ItemsBin, code:=CodeBin,
                        start:={datetime, Start}, deadline:={datetime, Deadline}} = Gift,
                      Sn = binary_to_list(SnBin),
                      NewGift = Gift#{sn => Sn,
                                      type => binary_to_list(TypeBin),
                                      items => util:bitstring_to_term(ItemsBin),
                                      code => binary_to_list(CodeBin),
                                      start => util:to_str_time(Start),
                                      deadline => util:to_str_time(Deadline)},
                      util:put_ets(gifts, Sn, NewGift)
              end,
    lists:foreach(GiftFun, GiftObjs),

    NotifyObjs = db:select_as_maps(<<"SELECT * FROM notifys">>),
    NotifyFun = fun(Notify) ->
                        #{id:=Id, svr_ids:=SvrIdsBin,
                          time:={datetime, Time}} = Notify,
                        SvrIds = util:bitstring_to_term(SvrIdsBin),
                        NewNotify = Notify#{svr_ids=>SvrIds, time=>util:to_str_time(Time)},
                        util:put_ets(notifys, Id, NewNotify)
                end,
    lists:foreach(NotifyFun, NotifyObjs),
    ok.
