-module(xyadmin_clt).
-compile(export_all).

-include("xyadmin.hrl").

-define(NGPUSH_HOST, "http://push.x.netease.com:53710").
-define(NGPUSH_SERVICE, "com.netease.ma37").
-define(NGPUSH_TOKEN, "5da0aab10").

get_url(SvrId) ->
    Serverlist = serverlist_lib:get_serverlist(SvrId),
    do_get_url(Serverlist).

do_get_url(Serverlist) ->
    #{ip:=Ip, local_ip:=LocalIp, gm_port:=GmPort} = Serverlist,
    Host = case LocalIp of
               "" -> Ip;
               <<>> -> Ip;
               _ -> LocalIp
           end,
    io_lib:format(<<"http://~s:~p">>, [util:to_list(Host), GmPort]).

send_sysmsg(SvrId, Sysmsg) ->
    Serverlist = serverlist_lib:get_serverlist(SvrId),
    #{ip:=Ip, local_ip:=LocalIp, gm_port:=GmPort} = Serverlist,
    Host = case LocalIp of
               "" -> Ip;
               <<>> -> Ip;
               _ -> LocalIp
           end,
    Method = post,
    URL = lists:concat(["http://", util:to_list(Host), ":" ,GmPort]),
    Headers = [],
    #{start_time:=StartTime} = Sysmsg,
    NewTime = util:strtime_to_unixtime(StartTime),
    NewSysmsg = Sysmsg#{start_time=>NewTime, action=>"gm_broadcast_msg"},
    Payload = jsx:encode(NewSysmsg),
    Options = [{pool, default}],
    case hackney:request(Method, URL, Headers, Payload, Options) of
        {ok, _StatusCode, _RespHanders, ClientRef} ->
            {ok, ResultBin} = hackney:body(ClientRef),
            Result = jsx:decode(ResultBin, [return_maps]),
            case maps:get(<<"status">>, Result) of
                <<"ok">> ->
                    ok;
                Status ->
                    lager:error("send_sysmsg fail, SvrId:~p, Sysmsg:~p, Status:~p", [SvrId, Sysmsg, Status]),
                    not_ok
            end;
        Result ->
            lager:error("send_sysmsg fail, SvrId:~p, Sysmsg:~p, Result:~p", [SvrId, Sysmsg, Result]),
            not_ok
    end.

send_email(SvrId, Email) ->
    URL = get_url(SvrId),
    Method = post,
    Headers = [],
    Payload = jsx:encode(Email#{action=>"send_mail"}),
    Options = [{pool, default}],
    case hackney:request(Method, URL, Headers, Payload, Options) of
        {ok, _StatusCode, _RespHanders, ClientRef} ->
            {ok, ResultBin} = hackney:body(ClientRef),
            Result = jsx:decode(ResultBin, [return_maps]),
            case maps:get(<<"status">>, Result) of
                <<"ok">> ->
                    ok;
                Status ->
                    lager:error("send_email fail, SvrId:~p, Sysmsg:~p, Status:~p", [SvrId, Email, Status]),
                    not_ok
            end;
        Result ->
            lager:error("send_email fail, SvrId:~p, Email:~p, Result:~p", [SvrId, Email, Result]),
            not_ok
    end.

lock_funcs(SvrId, LockedFuncs) ->
    URL = get_url(SvrId),
    Method = post,
    Headers = [],
    Payload = jsx:encode(#{action=>"close_functions", close_function_ids=>LockedFuncs}),
    Options = [{pool, default}],
    case hackney:request(Method, URL, Headers, Payload, Options) of
        {ok, _StatusCode, _RespHanders, ClientRef} ->
            {ok, ResultBin} = hackney:body(ClientRef),
            Result = jsx:decode(ResultBin, [return_maps]),
            case maps:get(<<"status">>, Result) of
                <<"ok">> ->
                    ok;
                Status ->
                    lager:error("lock_funcs fail, SvrId:~p, LockedFuncs:~p, Status:~p", [SvrId, LockedFuncs, Status]),
                    not_ok
            end;
        Result ->
            lager:error("lock_funcs fail, SvrId:~p, LockedFuncs:~p, Result:~p", [SvrId, LockedFuncs, Result]),
            not_ok
    end.

send_gift_svr(Serverlist, Payload) ->
    URL = do_get_url(Serverlist),
    Method = post,
    Headers = [],
    Options = [{pool, default}],
    case hackney:request(Method, URL, Headers, Payload, Options) of
        {ok, _StatusCode, _RespHanders, ClientRef} ->
            {ok, ResultBin} = hackney:body(ClientRef),
            case ResultBin of
                <<>> ->
                    lager:error("send_gift_svr fail, target svr raise error, SvrId:~p", [maps:get(svr_id, Serverlist)]),
                    not_ok;
                _ ->
                    Result = jsx:decode(ResultBin, [return_maps]),
                    case maps:get(<<"status">>, Result) of
                        <<"ok">> ->
                            ok;
                        "ok" ->
                            ok;
                        Status ->
                            lager:error("send_gift_svr fail, SvrId:~p, Status:~p", [maps:get(svr_id, Serverlist), Status]),
                            not_ok
                    end
            end;
        Result ->
            lager:error("send_gift_svr fail, SvrId:~p, Result:~p", [maps:get(svr_id, Serverlist), Result]),
            not_ok
    end.

send_activity_svr(Serverlist, ActivityInfo) ->
    Payload = jsx:encode(#{action=>"handle_activity", activity_info=>ActivityInfo}),
    URL = do_get_url(Serverlist),
    Method = post,
    Headers = [],
    Options = [{pool, default}],
    case hackney:request(Method, URL, Headers, Payload, Options) of
        {ok, _StatusCode, _RespHanders, ClientRef} ->
            {ok, ResultBin} = hackney:body(ClientRef),
            case ResultBin of
                <<>> ->
                    lager:error("send_activity_svr fail, target svr raise error, SvrId:~p", [maps:get(svr_id, Serverlist)]),
                    not_ok;
                _ ->
                    Result = jsx:decode(ResultBin, [return_maps]),
                    case maps:get(<<"status">>, Result) of
                        <<"ok">> ->
                            ok;
                        "ok" ->
                            ok;
                        Status ->
                            lager:error("send_activity_svr fail, SvrId:~p, Status:~p", [maps:get(svr_id, Serverlist), Status]),
                            not_ok
                    end
            end;
        Result ->
            lager:error("send_activity_svr fail, SvrId:~p, Result:~p", [maps:get(svr_id, Serverlist), Result]),
            not_ok
    end.

cancel_activity(Serverlist, ActivityId) ->
    Payload = jsx:encode(#{action=>"cancel_activity", activity_id=>ActivityId}),
    URL = do_get_url(Serverlist),
    Method = post,
    Headers = [],
    Options = [{pool, default}],
    case hackney:request(Method, URL, Headers, Payload, Options) of
        {ok, _StatusCode, _RespHanders, ClientRef} ->
            {ok, ResultBin} = hackney:body(ClientRef),
            case ResultBin of
                <<>> ->
                    lager:error("cancel_activity fail, target svr raise error, SvrId:~p", [maps:get(svr_id, Serverlist)]),
                    not_ok;
                _ ->
                    Result = jsx:decode(ResultBin, [return_maps]),
                    case maps:get(<<"status">>, Result) of
                        <<"ok">> ->
                            ok;
                        "ok" ->
                            ok;
                        Status ->
                            lager:error("cancel_activity fail, SvrId:~p, Status:~p", [maps:get(svr_id, Serverlist), Status]),
                            not_ok
                    end
            end;
        Result ->
            lager:error("cancel_activity fail, SvrId:~p, Result:~p", [maps:get(svr_id, Serverlist), Result]),
            not_ok
    end.


send_notify(SvrId, NotifyInfo) ->
    URL = get_url(SvrId),
    Payload = jsx:encode(NotifyInfo#{action => "send_notify"}),
    Method = post,
    Headers = [],
    Options = [{pool, default}],
    case hackney:request(Method, URL, Headers, Payload, Options) of
        {ok, _StatusCode, _RespHanders, ClientRef} ->
            {ok, ResultBin} = hackney:body(ClientRef),
            case ResultBin of
                <<>> ->
                    lager:error("send_notify fail, target svr raise error, SvrId:~p", [SvrId]),
                    not_ok;
                _ ->
                    Result = jsx:decode(ResultBin, [return_maps]),
                    case maps:get(<<"status">>, Result) of
                        <<"ok">> ->
                            ok;
                        "ok" ->
                            ok;
                        Status ->
                            lager:error("send_notify fail, SvrId:~p, Status:~p", [SvrId, Status]),
                            not_ok
                    end
            end;
        Result ->
            lager:error("send_notify fail, SvrId:~p, Result:~p", [SvrId, Result]),
            not_ok
    end.

open_guild_dungeon(SvrId) ->
    URL = get_url(SvrId),
    Method = post,
    Headers = [],
    Payload = jsx:encode(#{action=>"open_guild_dungeon"}),
    Options = [{pool, default}],
    case hackney:request(Method, URL, Headers, Payload, Options) of
        {ok, _StatusCode, _RespHanders, ClientRef} ->
            {ok, ResultBin} = hackney:body(ClientRef),
            Result = jsx:decode(ResultBin, [return_maps]),
            case maps:get(<<"status">>, Result) of
                <<"ok">> ->
                    ok;
                Status ->
                    lager:error("open_guild_dungeon fail, SvrId:~p, Status:~p", [SvrId, Status]),
                    not_ok
            end;
        Result ->
            lager:error("open_guild_dungeon fail, SvrId:~p, Result:~p", [SvrId, Result]),
            not_ok
    end.

close_guild_dungeon(SvrId) ->
    URL = get_url(SvrId),
    Method = post,
    Headers = [],
    Payload = jsx:encode(#{action=>"close_guild_dungeon"}),
    Options = [{pool, default}],
    case hackney:request(Method, URL, Headers, Payload, Options) of
        {ok, _StatusCode, _RespHanders, ClientRef} ->
            {ok, ResultBin} = hackney:body(ClientRef),
            Result = jsx:decode(ResultBin, [return_maps]),
            case maps:get(<<"status">>, Result) of
                <<"ok">> ->
                    ok;
                Status ->
                    lager:error("close_guild_dungeon fail, SvrId:~p, Status:~p", [SvrId, Status]),
                    not_ok
            end;
        Result ->
            lager:error("close_guild_dungeon fail, SvrId:~p, Result:~p", [SvrId, Result]),
            not_ok
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
send_ngpush_notify(NotifyInfo) ->
    #{title:=Title, content:=Content, is_and:=IsAnd, svr_ids:=SvrIds, is_pay:=IsPay, levels:=Levels} = NotifyInfo,
    {Tag, TagAnd} = format_tag(SvrIds, IsAnd, IsPay, Levels),
    push_sth(Tag, TagAnd, unicode:characters_to_binary(list_to_binary(Title)), unicode:characters_to_binary(list_to_binary(Content))).
   

format_tag(SvrIds, IsAnd, IsPay, Levels) ->
    ServerTags = [string:join(["server" ++ integer_to_list(SvrId) || SvrId <- SvrIds], ",")],

    PayTags = case IsPay of
                     1 -> ["is_pay"];
                     0 -> []
                 end,
    LevelsTags = ["level" ++ integer_to_list(Level) || Level <- Levels],
    MergeTags = string:join(ServerTags ++ PayTags ++ LevelsTags, ","),
    case IsAnd of
        1 -> {"", MergeTags};
        0 -> {MergeTags, ""}
    end.

push_sth(Tag, TagAnd, Title, Something) when Tag == "" andalso TagAnd == "" ->
    push_sth("npqj", TagAnd, Title, Something);
push_sth(Tag, TagAnd, Title, Something) when Tag == "" ->
    Params = [{tag_and, TagAnd}, {msg, unicode:characters_to_binary(Something)}, {service, ?NGPUSH_SERVICE}, {title, unicode:characters_to_binary(Title)}],
    Path = "/push?" ++ mochiweb_util:urlencode(Params),
    ?TRACE_VAR(Path),
    Headers = gen_headers(Path),
    handle_http(Path, Headers, push_sth);
push_sth(Tag, TagAnd, Title, Something) when TagAnd == "" ->
    Params = [{tag, Tag}, {msg, unicode:characters_to_binary(Something)}, {service, ?NGPUSH_SERVICE}, {title, unicode:characters_to_binary(Title)}],
    Path = "/push?" ++ mochiweb_util:urlencode(Params),
    ?TRACE_VAR(Path),
    Headers = gen_headers(Path),
    handle_http(Path, Headers, push_sth).

gen_headers(Path) ->
    StrTimestamp = integer_to_list(util:unixtime()),
    StrAuth = util:sha1_hex(Path ++ StrTimestamp ++ ?NGPUSH_TOKEN),
    [%{<<"Content-Type">>, <<"application/x-www-form-urlencoded; charset=utf-8">>},
     {<<"Auth">>, StrAuth},
     {<<"Timestamp">>, StrTimestamp}].

handle_http(Path, Headers, FunName) ->
    case hackney:request(get, ?NGPUSH_HOST ++ Path, Headers, <<>>, [{pool, default}]) of
        {ok, _StatusCode, _RespHeaders, ClientRef} ->
            {ok, Body} = hackney:body(ClientRef),
            Result = jsx:decode(Body, [return_maps]),
            ?TRACE_VAR(Result),
            case maps:get(<<"status">>, Result) of
                200 ->
                    ok;
                _ ->
                    lager:error("NgPush ~p result error, Result:~p", [FunName, Result]),
                    Result
            end;
        Result ->
            lager:error("NgPush ~p http error, Reason:~p", [FunName, Result]),
            Result
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
