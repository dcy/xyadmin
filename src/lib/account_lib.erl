-module(account_lib).
-compile(export_all).

-include("xyadmin.hrl").

hash_password(Password, Salt) ->
    mochihex:to_hex(erlang:md5(Salt ++ Password)).

hash_for(Name, Password) ->
    Salt = mochihex:to_hex(erlang:md5(Name)),
    hash_password(Password, Salt).

is_login(Req) ->
    case Req:cookie("account_id") of
        undefined -> false;
        [] -> false;
        AccountId -> AccountId
    end.

is_login_cookie(Req) ->
    case is_login(Req) of
        false ->
            false;
        AccountId ->
            case get_account(AccountId) of
                undefined ->
                    false;
                Account ->
                    case session_identifier(maps:get(id, Account)) =:= Req:cookie("session_id") of
                        false -> false;
                        true -> Account 
                    end
            end
    end.

require_login(Req) ->
    case is_login_cookie(Req) of
        false ->
            {redirect, "/account/login"};
        Account ->
            {ok, Account}
    end.

js_require_login(Req) ->
    case is_login_cookie(Req) of
        false ->
            {json, [{result, "not_login"}]};
        Account ->
            {ok, Account}
    end.

get_account(OriAccountId) ->
    AccountId = util:get_right_id(OriAccountId),
    case util:get_ets(accounts, AccountId) of
        undefined ->
            Result = db:select_as_maps(<<"SELECT * FROM accounts WHERE id=?">>, [AccountId]),
            case Result of
                [] -> 
                    undefined;
                [Account] ->
                    util:put_ets(accounts, AccountId, Account),
                    Account
            end;
        Account ->
            Account
    end.

get_name(AccountId) ->
    case get_account(AccountId) of
        undefined -> "";
        Account -> maps:get(name, Account)
    end.

update_cache_account(Account) ->
    util:put_ets(accounts, maps:get(id, Account), Account).

session_identifier(Id) ->
    %mochihex:to_hex(erlang:md5(?SECRET_STRING ++ Id)).
    mochihex:to_hex(erlang:md5(lists:concat([?SECRET_STRING, Id]))).

check_password(Password, PasswordHash) ->
    Salt = mochihex:to_hex(erlang:md5(?SECRET_STRING)),
    case is_binary(PasswordHash) of
        true ->
            account_lib:hash_password(Password, Salt) =:= erlang:binary_to_list(PasswordHash);
        false ->
            account_lib:hash_password(Password, Salt) =:= PasswordHash
    end.


login_cookies(Id) ->
    [mochiweb_cookies:cookie("account_id", Id, [{path, "/"}]),
     mochiweb_cookies:cookie("session_id", session_identifier(Id), [{path, "/"}])].

login_cookies(Req, Id) ->
    Req1 = cowboy_req:set_resp_cookie(<<"account_id">>, Id, [], Req),
    Req2 = cowboy_req:set_resp_cookie(<<"session_id">>, session_identifier(Id), [], Req1),
    [Req1, Req2].


