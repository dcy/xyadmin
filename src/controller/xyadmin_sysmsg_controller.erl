-module(xyadmin_sysmsg_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    case Action of
        "list" -> account_lib:require_login(Req);
        _ -> account_lib:js_require_login(Req)
    end.

list('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"sysmsg", tail=>"list"}}]}.

send('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"sysmsg", tail=>"send"}}]};

send('POST', [], _Account) ->
    MsgType = list_to_integer(Req:post_param("type")),
    StartTime = Req:post_param("start_time"),
    Times = list_to_integer(Req:post_param("times")),
    IntervalMinute = list_to_integer(Req:post_param("interval_minute")),
    StrSvrIds = Req:param_group("serverlists[]"),
    SvrIds = [list_to_integer(StrSvrId) || StrSvrId <- StrSvrIds, StrSvrId =/= ""],
    Content = Req:post_param("content"),
    Sysmsg = #{msg_type=>MsgType, start_time=>StartTime, times=>Times,
               interval_minute=>IntervalMinute, content=>Content},
    %[xyadmin_clt:send_sysmsg(SvrId, Sysmsg) || SvrId <- SvrIds],
    Fun = fun(SvrId, Svrs) ->
                  case xyadmin_clt:send_sysmsg(SvrId, Sysmsg) of
                      ok -> Svrs;
                      not_ok -> [serverlist_lib:get_simple_info(SvrId) | Svrs]
                  end
          end,
    FailSvrs = lists:foldl(Fun, [], SvrIds),
    LastId = db:execute(<<"INSERT INTO sysmsgs(msg_type, start_time, times, interval_minute, content, svr_ids) VALUES (?, ?, ?, ?, ?, ?)">>,
                        [MsgType, StartTime, Times, IntervalMinute, Content, util:term_to_bitstring(SvrIds)]),
    util:put_ets(sysmsgs, LastId, Sysmsg#{id=>LastId, svr_ids=>SvrIds}),
    {json, [{fail_svrs, FailSvrs}]}.

get_sysmsgs('GET', [], _Account) ->
    AllSysmsgs = [Sysmsg || {_, Sysmsg} <- ets:tab2list(sysmsgs)],
    SortFun = fun(FSysmsg, BSysmsg) ->
                      maps:get(id, FSysmsg) >= maps:get(id, BSysmsg)
              end,
    SortSysmsgs = lists:sort(SortFun, AllSysmsgs),
    Fun = fun(Sysmsg) ->
                  #{msg_type:=TypeId, svr_ids:=SvrIds} = Sysmsg,
                  Type = maps:to_list(util:mapskeyfind(TypeId, value, ?SYSMSG_TYPES)),
                  SvrFun = fun(SvrId) ->
                                   ?TRACE_VAR(SvrId),
                                   Serverlist = serverlist_lib:get_serverlist(SvrId),
                                   case Serverlist of
                                       undefined ->
                                           [{svr_id, 0}, {name, unicode:characters_to_binary("已删除")}];
                                       _ ->
                                           #{name:=Name} = Serverlist,
                                           [{svr_id, SvrId}, {name, Name}]
                                   end
                           end,
                  Svrs = lists:map(SvrFun, SvrIds),
                  NewSysMsg = Sysmsg#{msg_type=>Type, svrs=>Svrs},
                  maps:to_list(maps:remove(players, NewSysMsg))
          end,
    Sysmsgs = lists:map(Fun, SortSysmsgs),
    {json, [{sysmsgs, Sysmsgs}]}.

    
del('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    db:async_execute(<<"DELETE FROM sysmsgs WHERE id=?">>, [Id]),
    util:del_ets(sysmsgs, Id),
    {json, [{result, "success"}]}.

