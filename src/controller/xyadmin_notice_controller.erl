-module(xyadmin_notice_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    account_lib:require_login(Req).

list('GET', [Type], Account) ->
    {ok, [{account, Account}, {type, Type}, {path, #{head=>"notice", tail=>Type}}]}.

add('GET', [Type], Account) ->
    {ok, [{account, Account}, {type, Type}, {path, #{head=>"notice", tail=>"qa"}}]};

add('POST', [Type], Account) ->
    Vsn = Req:post_param("vsn"),
    StrPlatforms = Req:param_group("platforms[]"),
    Platforms = [list_to_integer(StrPlatform) || StrPlatform <- StrPlatforms],
    Content = Req:post_param("content"),
    Id = db:execute(<<"INSERT INTO notices(type, vsn, platforms, content) VALUES(?, ?, ?, ?)">>,
                    [Type, Vsn, util:term_to_bitstring(Platforms), Content]),
    Notice = #{id=>Id, vsn=>Vsn, type=>Type, platforms=>Platforms, content=>Content},
    util:put_ets(notices, Id, Notice),
    notice_lib:save_notices_files(),
    {json, [{result, "success"}]}.

get_notices('GET', [Type], _Account) ->
    TypeNotices = notice_lib:get_notices(Type),
    Fun = fun(Notice) ->
                  #{platforms:=PlatformIds} = Notice,
                  PlatformFun = fun(PlatformId) ->
                                        maps:to_list(util:mapskeyfind(PlatformId, value, ?PLATFORMS))
                                end,
                  Platforms = lists:map(PlatformFun, PlatformIds),
                  NewNotice = Notice#{platforms=>Platforms},
                  maps:to_list(NewNotice)
          end,
    Notices = lists:map(Fun, TypeNotices),
    {json, [{notices, Notices}]}.

del('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    ?TRACE_VAR(Id),
    db:async_execute(<<"DELETE FROM notices WHERE id=?">>, [Id]),
    util:del_ets(notices, Id),
    notice_lib:save_notices_files(),
    {json, [{result, "success"}]}.

edit('GET', [Type], Account) ->
    {ok, [{account, Account}, {type, Type}, {path, #{head=>"notice", tail=>Type}}]};

edit('POST', [Type], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    Vsn = Req:post_param("vsn"),
    StrPlatforms = Req:param_group("platforms[]"),
    Platforms = [list_to_integer(StrPlatform) || StrPlatform <- StrPlatforms],
    Content = Req:post_param("content"),
    case util:get_ets(notices, Id) of
        undefined ->
            {json, [{result, "fail"}]};
        _ ->
            db:execute(<<"UPDATE notices set vsn=?, platforms=?, content=? WHERE id=?">>,
                       [Vsn, util:term_to_bitstring(Platforms), Content, Id]),
            Notice = #{id=>Id, vsn=>Vsn, type=>Type, platforms=>Platforms, content=>Content},
            util:put_ets(notices, Id, Notice),
            notice_lib:save_notices_files(),
            {json, [{result, "success"}]}
    end.
