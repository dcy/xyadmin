-module(xyadmin_function_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    account_lib:require_login(Req).

list('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"function", tail=>"list"}}]}.

lock('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"function", tail=>"lock"}}]};

lock('POST', [], _Account) ->
    StrSvrIds = Req:param_group("svr_ids[]"),
    SvrIds = [list_to_integer(StrSvrId) || StrSvrId <- StrSvrIds, StrSvrId =/= ""],
    StrLockedFuncs = Req:param_group("locked_funcs[]"),
    LockedFuncs = [list_to_integer(StrLockedFunc) || StrLockedFunc <- StrLockedFuncs, StrLockedFunc =/= ""],
    Fun = fun(SvrId, Svrs) ->
                  case lock_lib:lock_funcs(SvrId, LockedFuncs) of
                      ok -> Svrs;
                      not_ok -> [serverlist_lib:get_simple_info(SvrId) | Svrs]
                  end
          end,
    FailSvrs = lists:foldl(Fun, [], SvrIds),
    {json, [{fail_svrs, FailSvrs}]}.
