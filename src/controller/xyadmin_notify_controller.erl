-module(xyadmin_notify_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    account_lib:require_login(Req).

list('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"notify", tail=>"list"}}]}.

send('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"notify", tail=>"send"}}]};

send('POST', [], Account) ->
    Title = Req:post_param("title"),
    Content = Req:post_param("content"),
    StrSvrIds= Req:param_group("serverlists[]"),
    SvrIds = [list_to_integer(SvrId) || SvrId <- StrSvrIds],
    IsAnd = format_str_bool_to_integer(Req:post_param("is_and")),
    IsPay = format_str_bool_to_integer(Req:post_param("is_pay")),
    LevelType = list_to_integer(Req:post_param("level_type")),
    LevelValue = list_to_integer(Req:post_param("level_value")),
    Args = #{title=>Title, content=>Content, is_and=>IsAnd, svr_ids=>SvrIds, is_pay=>IsPay, levels=>format_levels(LevelType, LevelValue)},
    case xyadmin_clt:send_ngpush_notify(Args) of
        ok ->
            SenderId = maps:get(id, Account),
            Now = calendar:local_time(),
            LastId = db:execute(<<"INSERT INTO notifys(sender_id, title, content, is_and, svr_ids, is_pay, level_type, level_value, time) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)">>,
                                [SenderId, Title, Content, IsAnd, util:term_to_bitstring(SvrIds), IsPay, LevelType, LevelValue, Now]),
            NewNotify = #{id=>LastId, sender_id=>SenderId, title=>Title, content=>Content,
                          is_and=>IsAnd, svr_ids=>SvrIds, is_pay=>IsPay, level_type=>LevelType,
                          level_value=>LevelValue,
                          time=>util:to_str_time(Now)},
            util:put_ets(notifys, LastId, NewNotify),
            
            {json, [{result, "success"}, {msg, ""}]};
        Error ->
            lager:error("send notify's error: ~p", [Error]),
            {json, [{result, "fail"}, {msg, maps:get(<<"err">>, Error)}]}
    end.

get_notifys('GET', [], _Account) ->
    AllNotifys = [Email || {_, Email} <- ets:tab2list(notifys)],
    Fun = fun(Notify) ->
                  #{svr_ids:=SvrIds} = Notify,
                  SvrFun = fun(SvrId) ->
                                   Serverlist = serverlist_lib:get_serverlist(SvrId),
                                   case Serverlist of
                                       undefined ->
                                           [{svr_id, 0}, {name, unicode:characters_to_binary("已删除")}];
                                       _ ->
                                           #{name:=Name} = Serverlist,
                                           [{svr_id, SvrId}, {name, Name}]
                                   end
                           end,
                  Svrs = case SvrIds of
                             "all" -> [{svr_id, 0}, {name, unicode:characters_to_binary("全部")}];
                             _ -> lists:map(SvrFun, SvrIds)
                         end,
                  NewNotify = Notify#{svrs=>Svrs},
                  maps:to_list(NewNotify)
          end,
    Notifys = lists:map(Fun, AllNotifys),
    {json, [{notifys, Notifys}]}.

    
format_str_bool_to_integer(Item) ->
    case Item of
        "false" -> 0;
        "true" -> 1
    end.

format_levels(LevelType, LevelValue) ->
    case LevelType of
        0 -> [];
        1 -> [LevelValue];
        2 -> [Level || Level <- lists:seq(LevelValue+1, 100)];
        3 -> [Level || Level <- lists:seq(1, LevelValue-1)]
    end.
