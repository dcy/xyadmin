-module(xyadmin_activity_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    account_lib:require_login(Req).

get_confs('GET', [], _Account) ->
    Activities = [maps:to_list(Activity) || {_Id, Activity} <- lists:keysort(1, ets:tab2list(activities))],
    {json, [{activities, Activities}]}.


list('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"activity", tail=>"list"}}]}.

send_specify_svrs('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    Start = Req:post_param("start"),
    Deadline = Req:post_param("deadline"),
    StrSvrIds = Req:param_group("svr_ids[]"),
    SvrIds = [list_to_integer(StrSvrId) || StrSvrId <- StrSvrIds, StrSvrId =/= ""],
    case util:unixtime() > util:strtime_to_unixtime(Deadline) of
        true ->
            {json, [{result, "isExpire"}]};
        false ->
            Activity = activity_lib:get_activity(Id),
            ?ASSERT(Activity =/= undefined, noThisIdActivity),
            ActivityInfo = #{id=>Id, start=>Start, deadline=>Deadline},
            FailSvrs = activity_lib:send_svrs(SvrIds, ActivityInfo),
            {json, [{result, "success"}, {fail_svrs, FailSvrs}]}
    end.

send_all_svrs('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    Start = Req:post_param("start"),
    Deadline = Req:post_param("deadline"),
    case util:unixtime() > util:strtime_to_unixtime(Deadline) of
        true ->
            {json, [{result, "isExpire"}]};
        false ->
            Activity = activity_lib:get_activity(Id),
            ?ASSERT(Activity =/= undefined, noThisIdActivity),
            ActivityInfo = #{id=>Id, start=>Start, deadline=>Deadline},
            FailSvrs = activity_lib:send_svrs(ActivityInfo),
            {json, [{result, "success"}, {fail_svrs, FailSvrs}]}
    end.

cancel_specify_svrs('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    StrSvrIds = Req:param_group("svr_ids[]"),
    SvrIds = [list_to_integer(StrSvrId) || StrSvrId <- StrSvrIds, StrSvrId =/= ""],
    Activity = activity_lib:get_activity(Id),
    ?ASSERT(Activity =/= undefined, noThisIdActivity),
    FailSvrs = activity_lib:cancel_svrs(SvrIds, Id),
    {json, [{result, "success"}, {fail_svrs, FailSvrs}]}.

cancel_all_svrs('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    Activity = activity_lib:get_activity(Id),
    ?ASSERT(Activity =/= undefined, noThisIdActivity),
    FailSvrs = activity_lib:cancel_svrs(Id),
    {json, [{result, "success"}, {fail_svrs, FailSvrs}]}.

info('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"activity", tail=>"info"}}]}.
