-module(xyadmin_serverlist_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    case Action of
        "list" -> account_lib:require_login(Req);
        "add" -> account_lib:require_login(Req);
        _ -> account_lib:js_require_login(Req)
    end.

list('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"serverlist", tail=>"list"}}]}.
    
get_serverlists('GET', [], _Account) ->
    {json, [{serverlists, serverlist_lib:get_format_serverlists()}]}.

get_simple_serverlists('GET', [], _Account) ->
    {json, [{serverlists, serverlist_lib:get_simple_serverlists()}]}.


add('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"serverlist", tail=>"add"}}]};

add('POST', [], Account) ->
    SvrId = list_to_integer(Req:post_param("svr_id")),
    Name = Req:post_param("name"),
    CltVsn = Req:post_param("clt_vsn"),
    AreaName = Req:post_param("area_name"),
    State = list_to_integer(Req:post_param("state")),
    Type = list_to_integer(Req:post_param("type")),
    Ip = Req:post_param("ip"),
    LocalIp = util:ensure_str(Req:post_param("local_ip")),
    Port = list_to_integer(Req:post_param("port")),
    GmPort = list_to_integer(Req:post_param("gm_port")),
    LogPort = list_to_integer(Req:post_param("log_port")),
    StrPlatforms = Req:param_group("platforms[]"),
    Platforms = [list_to_integer(StrPlatform) || StrPlatform <- StrPlatforms, StrPlatform =/= ""],
    StrChannels = Req:param_group("channels[]"),
    Channels = [list_to_integer(StrChannel) || StrChannel <- StrChannels, StrChannel =/= ""],
    CltUpdateUrl = Req:post_param("clt_update_url"),
    ActivityListId = case util:ensure_str(Req:post_param("activity_list_id")) of
                         "" -> 0;
                         TheActivityListId -> list_to_integer(TheActivityListId)
                     end,

    case util:get_ets(serverlists, SvrId) of
        undefined ->
            db:execute(<<"INSERT INTO serverlists(svr_id, name, clt_vsn, area_name, state,
                               type, ip, local_ip, port, gm_port, log_port,
                               platforms, channels, clt_update_url, activity_list_id, activity_infos) VALUES
                               (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)">>,
                       [SvrId, Name, CltVsn, AreaName, State, Type, Ip, LocalIp, Port, GmPort,
                        LogPort, util:term_to_bitstring(Platforms), util:term_to_bitstring(Channels), CltUpdateUrl, ActivityListId, "[]"]),
            Serverlist = #{svr_id=>SvrId, name=>Name, clt_vsn=>CltVsn, area_name=>AreaName,
                           state=>State, type=>Type, ip=>Ip, local_ip=>LocalIp, port=>Port,
                           gm_port=>GmPort, log_port=>LogPort, platforms=>Platforms,
                           channels=>Channels, clt_update_url=>CltUpdateUrl,
                           activity_list_id=>ActivityListId, locked_funcs=>[], gift_sns=>[],
                           activity_infos=>[]},
            util:put_ets(serverlists, SvrId, Serverlist),
            serverlist_lib:save_serverlists_files(Platforms, Channels),
            {json, [{result, "success"}]};
        _ ->
            {json, [{result, "svrIdExist"}]}
    end.

del('POST', [], Account) ->
    SvrId = list_to_integer(Req:post_param("svr_id")),
    Serverlist = serverlist_lib:get_serverlist(SvrId),
    #{platforms:=PlatformIds, channels:=ChannelIds} = Serverlist,
    db:async_execute(<<"DELETE FROM serverlists WHERE svr_id=?">>, [SvrId]),
    util:del_ets(serverlists, SvrId),
    serverlist_lib:save_serverlists_files(PlatformIds, ChannelIds),
    {json, [{result, "success"}]}.

edit('GET', [SvrIdStr], Account) ->
    {ok, [{account, Account}, {svr_id, list_to_integer(SvrIdStr)}, {path, #{head=>"serverlist"}}]};

edit('POST', [], _Account) ->
    SvrId = list_to_integer(Req:post_param("svr_id")),
    OriServerlist = serverlist_lib:get_serverlist(SvrId),
    #{locked_funcs:=LockedFuncs, gift_sns:=GiftSns, activity_infos:=ActivityInfos} = OriServerlist,
    Name = Req:post_param("name"),
    CltVsn = Req:post_param("clt_vsn"),
    AreaName = Req:post_param("area_name"),
    State = list_to_integer(Req:post_param("state")),
    Type = list_to_integer(Req:post_param("type")),
    Ip = Req:post_param("ip"),
    LocalIp = case Req:post_param("local_ip") of
                  undefined -> "";
                  TheLocalIp -> TheLocalIp
              end,
    LocalIp = util:ensure_str(Req:post_param("local_ip")),
    Port = list_to_integer(Req:post_param("port")),
    GmPort = list_to_integer(Req:post_param("gm_port")),
    LogPort = list_to_integer(Req:post_param("log_port")),
    StrPlatforms = Req:param_group("platforms[]"),
    Platforms = [list_to_integer(StrPlatform) || StrPlatform <- StrPlatforms, StrPlatform =/= ""],
    StrChannels = Req:param_group("channels[]"),
    Channels = [list_to_integer(StrChannel) || StrChannel <- StrChannels, StrChannel =/= ""],
    CltUpdateUrl = util:ensure_str(Req:post_param("clt_update_url")),
    ActivityListId = list_to_integer(Req:post_param("activity_list_id")),
    case util:get_ets(serverlists, SvrId) of
        undefined ->
            {json, [{result, "svrIdNotExist"}]};
        _ ->
            OriServerlist = serverlist_lib:get_serverlist(SvrId),
            #{platforms:=OriPlatforms, channels:=OriChannels} = OriServerlist,
            db:execute(<<"REPLACE INTO serverlists(svr_id, name, clt_vsn, area_name, state,
                               type, ip, local_ip, port, gm_port, log_port,
                               platforms, channels, clt_update_url, activity_list_id, locked_funcs, gift_sns, activity_infos) VALUES
                               (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)">>,
                       [SvrId, Name, CltVsn, AreaName, State, Type, Ip, LocalIp, Port, GmPort,
                        LogPort, util:term_to_bitstring(Platforms), util:term_to_bitstring(Channels), CltUpdateUrl, ActivityListId,
                        util:term_to_bitstring(LockedFuncs), util:term_to_bitstring(GiftSns), util:term_to_bitstring(ActivityInfos)]),
            Serverlist = #{svr_id=>SvrId, name=>Name, clt_vsn=>CltVsn, area_name=>AreaName,
                           state=>State, type=>Type, ip=>Ip, local_ip=>LocalIp, port=>Port,
                           gm_port=>GmPort, log_port=>LogPort, platforms=>Platforms,
                           channels=>Channels, clt_update_url=>CltUpdateUrl,
                           activity_list_id=>ActivityListId, locked_funcs=>LockedFuncs,
                           gift_sns=>GiftSns, activity_infos=>ActivityInfos
                          },
            util:put_ets(serverlists, SvrId, Serverlist),
            serverlist_lib:save_serverlists_files(util:merge_set_list(OriPlatforms, Platforms), util:merge_set_list(OriChannels, Channels)),
            {json, [{result, "success"}]}
    end.

get('GET', [SvrIdStr], _Account) ->
    SvrId = list_to_integer(SvrIdStr),
    Serverlist = serverlist_lib:get_serverlist(SvrId),
    {json, [{serverlist, serverlist_lib:format_serverlist(Serverlist)}]}.

