-module(xyadmin_gms_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    account_lib:require_login(Req).
        
list('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"gms", tail=>"list"}}]}.

open_guild_dungeon('POST', [], Account) ->
    StrSvrIds= Req:param_group("svr_ids[]"),
    SvrIds = [list_to_integer(SvrId) || SvrId <- StrSvrIds],
    Fun = fun(SvrId, Svrs) ->
                  case xyadmin_clt:open_guild_dungeon(SvrId) of
                      ok -> Svrs;
                      not_ok -> [serverlist_lib:get_simple_info(SvrId) | Svrs]
                  end
          end,
    FailSvrs = lists:foldl(Fun, [], SvrIds),
    {json, [{fail_svrs, FailSvrs}]}.

close_guild_dungeon('POST', [], Account) ->
    StrSvrIds= Req:param_group("svr_ids[]"),
    SvrIds = [list_to_integer(SvrId) || SvrId <- StrSvrIds],
    Fun = fun(SvrId, Svrs) ->
                  case xyadmin_clt:close_guild_dungeon(SvrId) of
                      ok -> Svrs;
                      not_ok -> [serverlist_lib:get_simple_info(SvrId) | Svrs]
                  end
          end,
    FailSvrs = lists:foldl(Fun, [], SvrIds),
    {json, [{fail_svrs, FailSvrs}]}.
