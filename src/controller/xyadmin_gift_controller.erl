-module(xyadmin_gift_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    account_lib:require_login(Req).

list('GET', [Type], Account) ->
    {ok, [{account, Account}, {type, Type}, {path, #{head=>"gift", tail=>Type}}]}.

add('GET', [Type], Account) ->
    {ok, [{account, Account}, {type, Type}, {path, #{head=>"gift", tail=>Type}}]};

add('POST', [Type], Account) ->
    Sn = string:strip(Req:post_param("sn"), both, $ ),
    Items = format_attachments(Req:param_group("items[]")),
    Code = string:strip(Req:post_param("code"), both, $ ),
    Amount = list_to_integer(Req:post_param("amount")),
    Start = Req:post_param("start"),
    Deadline = Req:post_param("deadline"),
    EmailTitle = Req:post_param("email_title"),
    EmailContent = Req:post_param("email_content"),
    Remark1 = Req:post_param("remark1"),
    Remark2 = Req:post_param("remark2"),
    case util:get_ets(gifts, Sn) of
        undefined ->
            case gift_lib:is_code_exist(Type, Code) of
                true ->
                    {json, [{result, "codeExist"}]};
                false ->
                    db:execute(<<"INSERT INTO gifts(sn, type, items, code, amount, start, deadline, email_title, email_content, remark1, remark2) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)">>,
                               [Sn, Type, util:term_to_bitstring(Items), Code, Amount, Start, Deadline, EmailTitle, EmailContent, Remark1, Remark2]),
                    Gift = #{sn=>Sn, type=>Type, items=>Items, code=>Code, amount=>Amount,
                             start=>Start, deadline=>Deadline, email_title=>EmailTitle,
                             email_content=>EmailContent, remark1=>Remark1, remark2=>Remark2},
                    util:put_ets(gifts, Sn, Gift),
                    gift_lib:gen_gift_codes(Type, Gift),
                    %gift_lib:refresh_erlang_files(),
                    {json, [{result, "success"}]}
            end;
        _ ->
            {json, [{result, "snExist"}]}
    end.

del('POST', [], _Account) ->
    Sn = Req:post_param("sn"),
    util:del_ets(gifts, Sn),
    db:execute(<<"DELETE FROM gifts WHERE sn=?">>, [Sn]),
    %gift_lib:refresh_erlang_files(),
    {json, [{result, "success"}]}.

erls('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"gift", tail=>"erls"}}]}.

refresh_erlang_files('POST', [], _Account) ->
    gift_lib:refresh_erlang_files(),
    {json, [{result, "success"}]}.


get_gifts('GET', [Type], _Account) ->
    TypeGifts = gift_lib:get_gifts(Type),
    Fun = fun(Gift) ->
                  #{items:=OriItems} = Gift,
                  Items = [maps:to_list(Item) || Item <- OriItems],
                  NewGift = Gift#{items => Items},
                  maps:to_list(NewGift)
          end,
    Gifts = lists:map(Fun, TypeGifts),
    {json, [{gifts, Gifts}]}.

get_data_conf('GET', [ConfName], _Account) ->
    ?TRACE_VAR(ConfName),
    FileName = lists:concat(["datas/", ConfName]),
    {ok, Data} = file:read_file(FileName),
    CodesBin = binary:split(Data, [<<"\n">>], [global]),
    Fun = fun(CodeBin) ->
                  ?TRACE_VAR(CodeBin),
                  [TidBin, NameBin] = binary:split(CodeBin, [<<",">>], [global]),
                  [{tid, binary_to_list(TidBin)}, {name, NameBin}]
          end,
    Items = [Fun(Code) || Code <- CodesBin, Code =/= <<>>],
    {json, [{item_infos, [[{tid, ""}, {name, unicode:characters_to_binary("请选择")}]] ++ Items}]}.

send_svrs('POST', [], _Account) ->
    Sn = Req:post_param("sn"),
    Gift = util:get_ets(gifts, Sn),
    #{deadline:=Deadline} = Gift,
    case util:unixtime() > util:strtime_to_unixtime(Deadline) of
        true ->
            {json, [{result, "isExpire"}]};
        false ->
            #{type:=Type} = Gift,
            FileName = lists:concat(["priv/static/gifts/", Sn, "_", Type, "_gift_code"]),
            {ok, CodeData} = file:read_file(FileName),
            FailSvrs = gift_lib:send_gift_svrs(Gift, CodeData),
            {json, [{result, "success"}, {fail_svrs, FailSvrs}]}
    end.

send_specify_svrs('POST', [], _Account) ->
    Sn = Req:post_param("sn"),
    StrSvrIds = Req:param_group("svr_ids[]"),
    SvrIds = [list_to_integer(StrSvrId) || StrSvrId <- StrSvrIds, StrSvrId =/= ""],
    Gift = util:get_ets(gifts, Sn),
    #{deadline:=Deadline} = Gift,
    case util:unixtime() > util:strtime_to_unixtime(Deadline) of
        true ->
            {json, [{result, "isExpire"}]};
        false ->
            #{type:=Type} = Gift,
            FileName = lists:concat(["priv/static/gifts/", Sn, "_", Type, "_gift_code"]),
            {ok, CodeData} = file:read_file(FileName),
            FailSvrs = gift_lib:send_gift_svrs(SvrIds, Gift, CodeData),
            {json, [{result, "success"}, {fail_svrs, FailSvrs}]}
    end.

info('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"gift", tail=>"info"}}]}.

get_detail('GET', [Sn], _Account) ->
    Gift = util:get_ets(gifts, Sn),
    #{items:=OriItems} = Gift,
    Items = [maps:to_list(Item) || Item <- OriItems],
    NewGift = Gift#{items => Items},
    {json, [{gift, maps:to_list(NewGift)}]}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format_attachments(AttachmentValStrs) ->
    Fun = fun(AttachmentValStr) ->
                  [ItemTypeOri, ItemTidOri, ItemAmountOri] = string:tokens(AttachmentValStr, "`"),
                  ItemType = list_to_integer(ItemTypeOri),
                  ItemTid = case ItemTidOri of
                               "null" -> 0;
                               _ -> list_to_integer(ItemTidOri)
                           end,
                  ItemAmount = list_to_integer(ItemAmountOri),
                  #{item_type=>ItemType, item_tid=>ItemTid, item_amount=>ItemAmount}
          end,
    lists:map(Fun, AttachmentValStrs).
