-module(xyadmin_account_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").


login('GET', []) ->
    case account_lib:is_login_cookie(Req) of
        false -> {ok, []};
        _ -> {redirect, "/"}
    end;

login('POST', []) ->
    Name = Req:post_param("username"),
    Password = Req:post_param("password"),
    case db:prepared_select_as_maps(<<"SELECT * FROM accounts WHERE name=?">>, [Name]) of
        [Account] ->
            #{id:=AccountId, password_hash:=PasswordHash} = Account,
            case account_lib:check_password(Password, PasswordHash) of
                true ->
                    Cookies = account_lib:login_cookies(AccountId),
                    {json, [{result, "success"}], Cookies};
                false ->
                    {json, [{result, "password_wrong"}]}
            end;
        [] ->
            {json, [{result, "user_not_exist"}]}
    end.

logout('GET', []) ->
    {redirect, "/account/login", [mochiweb_cookies:cookie("account_id", "", [{path, "/"}]),
                                  mochiweb_cookies:cookie("session_id", "", [{path, "/"}])]}.
