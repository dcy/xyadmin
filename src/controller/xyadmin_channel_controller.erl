-module(xyadmin_channel_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    case Action of
        "list" -> account_lib:require_login(Req);
        "add" -> account_lib:require_login(Req);
        _ -> account_lib:js_require_login(Req)
    end.

list('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"channel", tail=>"list"}}]}.
    
get_channels('GET', [], _Account) ->
    {json, [{channels, channel_lib:get_format_channels()}]}.

add('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"channel", tail=>"add"}}]};

add('POST', [], Account) ->
    Name = Req:post_param("name"),
    Tag = Req:post_param("tag"),
    try
        LastId = db:execute(<<"INSERT INTO channels(name, tag) VALUES(?, ?)">>, [Name, Tag]),
        Channel = #{id=>LastId, name=>Name, tag=>Tag},
        util:put_ets(channels, LastId, Channel),
        {json, [{result, "success"}]}
    catch
        Type:Reason ->
            lager:error("add_channel error's type:~p, reason:~p, stacktrace:~p", [Type, Reason, erlang:get_stacktrace()]),
            {json, [{result, "fail"}]}
    end.

del('POST', [], Account) ->
    Id = list_to_integer(Req:post_param("id")),
    case serverlist_lib:has_channel(Id) of
        undefined ->
            db:async_execute(<<"DELETE FROM channels WHERE id=?">>, [Id]),
            util:del_ets(channels, Id),
            {json, [{result, "success"}]};
        Serverlist ->
            #{svr_id:=SvrId, name:=Name} = Serverlist,
            {json, [{result, "fail"}, {serverlist, [{svr_id, SvrId}, {name, Name}]}]}
    end.
