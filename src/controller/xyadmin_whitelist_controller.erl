-module(xyadmin_whitelist_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    account_lib:require_login(Req).
        
list('GET', [Type], Account) ->
    {ok, [{account, Account}, {type, Type}, {path, #{head=>"whitelist", tail=>Type}}]}.

add('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"whitelist", tail=>"account"}}]};

add('POST', [], Account) ->
    AccountId = Req:post_param("account_id"),
    Username = Req:post_param("username"),
    IsAdvanced = case Req:post_param("is_advanced") of
                     "false" -> 0;
                     "true" -> 1
                 end,
    try
        Id = db:execute(<<"INSERT INTO whitelists(account_id, username, is_advanced) VALUES(?, ?, ?)">>, [AccountId, Username, IsAdvanced]),
        Whitelist = #{id=>Id, account_id=>AccountId, username=>Username, is_advanced=>IsAdvanced},
        util:put_ets(whitelists, Id, Whitelist),
        whitelist_lib:save_whitelists_file(),
        {json, [{result, "success"}]}
    catch
        Type:Reason ->
            %lager:error("add whitelist error's type:~p, reason:~p, stacktrace:~p", [Type, Reason, erlang:get_stacktrace()]),
            {json , [{result, "fail"}]}
    end.

add_udid('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"whitelist", tail=>"udid"}}]};

add_udid('POST', [], Account) ->
    Udid = Req:post_param("udid"),
    Remark = Req:post_param("remark"),
    try
        Id = db:execute(<<"INSERT INTO udid_whitelists(udid, remark) VALUES(?, ?)">>, [Udid, Remark]),
        Whitelist = #{id=>Id, udid=>Udid, remark=>Remark},
        util:put_ets(udid_whitelists, Id, Whitelist),
        whitelist_lib:save_udid_whitelists_file(),
        {json, [{result, "success"}]}
    catch
        Type:Reason ->
            lager:error("add whitelist error's type:~p, reason:~p, stacktrace:~p", [Type, Reason, erlang:get_stacktrace()]),
            {json, [{result, "fail"}]}
    end.

get_whitelists('GET', [Type], _Account) ->
    case Type of
        "account" ->
            Whitelists = [maps:to_list(Whitelist) || Whitelist <- whitelist_lib:get_sorted_whitelists()],
            {json, [{whitelists, Whitelists}]};
        "udid" ->
            Whitelists = [maps:to_list(Whitelist) || Whitelist <- whitelist_lib:get_sorted_udid_whitelists()],
            {json, [{whitelists, Whitelists}]}

    end.
    
del('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    db:async_execute(<<"DELETE FROM whitelists WHERE id=?">>, [Id]),
    util:del_ets(whitelists, Id),
    whitelist_lib:save_whitelists_file(),
    {json, [{result, "success"}]}.

del_udid('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    db:async_execute(<<"DELETE FROM udid_whitelists WHERE id=?">>, [Id]),
    util:del_ets(udid_whitelists, Id),
    whitelist_lib:save_udid_whitelists_file(),
    {json, [{result, "success"}]}.

update('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    AccountId = Req:post_param("account_id"),
    Username = Req:post_param("username"),
    IsAdvanced = case Req:post_param("is_advanced") of
                     "false" -> 0;
                     "true" -> 1
                 end,
    try
        db:execute(<<"UPDATE whitelists SET account_id=?, username=?, is_advanced=? WHERE id=?">>,
                   [AccountId, Username, IsAdvanced, Id]),
        Whitelist = #{id=>Id, account_id=>AccountId, username=>Username, is_advanced=>IsAdvanced},
        util:put_ets(whitelists, Id, Whitelist),
        whitelist_lib:save_whitelists_file(),
        {json, [{result, "success"}]}
    catch
        Type:Reason ->
            lager:error("update whitelist error's type:~p, reason:~p, stacktrace:~p",
                        [Type, Reason, erlang:get_stacktrace()]),
            {json, [{result, "fail"}]}
    end.

