-module(xyadmin_email_controller, [Req]).
-compile(export_all).
-include("xyadmin.hrl").

before_(Action, Arg1, Arg2) ->
    account_lib:require_login(Req).

list('GET', [], Account) ->
    {ok, [{account, Account}, {path, #{head=>"email", tail=>"list"}}]}.

send('GET', [Type], Account) ->
    {ok, [{account, Account}, {type, Type}, {path, #{head=>"email", tail=>Type}}]};

send('POST', [Type], Account) ->
    ?TRACE_VAR(Req:request_body()),
    Title = Req:post_param("title"),
    Attachments = format_attachments(Req:param_group("attachments[]")),
    StrUsers = Req:post_param("users"),
    StrUsersList = string:tokens(StrUsers, ","),
    Users = [list_to_integer(StrUser) || StrUser <- StrUsersList, StrUser =/= ""],
    StrSvrIds= Req:param_group("serverlists[]"),
    SvrIds = [list_to_integer(StrSvrId) || StrSvrId <- StrSvrIds, StrSvrId =/= ""],
    Content = Req:post_param("content"),
    Email = #{players=>Users, type=>2, title=>Title, content=>Content, items=>Attachments},
    %[xyadmin_clt:send_email(SvrId, Email) || SvrId <- SvrIds],
    Fun = fun(SvrId, Svrs) ->
                  case xyadmin_clt:send_email(SvrId, Email) of
                      ok -> Svrs;
                      not_ok -> [serverlist_lib:get_simple_info(SvrId) | Svrs]
                  end
          end,
    FailSvrs = lists:foldl(Fun, [], SvrIds),
    SenderId = maps:get(id, Account),
    LastId = db:execute(<<"INSERT INTO emails(sender_id, type, title, attachments, svr_ids, users, content) VALUES(?, ?, ?, ?, ?, ?, ?)">>,
                        [SenderId, Type, Title, util:term_to_bitstring(Attachments), util:term_to_bitstring(SvrIds), util:term_to_bitstring(Users), Content]),
    NewEmail = #{id=>LastId, users=>Users, sender_id=>SenderId, type=>Type, svr_ids=>SvrIds,
                 attachments=>Attachments, title=>Title, content=>Content},
    util:put_ets(emails, LastId, NewEmail),
    {json, [{fail_svrs, FailSvrs}]}.

del('POST', [], _Account) ->
    Id = list_to_integer(Req:post_param("id")),
    db:async_execute(<<"DELETE FROM emails WHERE id=?">>, [Id]),
    util:del_ets(emails, Id),
    {json, [{result, "success"}]}.

get_emails('GET', [], _Account) ->
    AllEmails = [Email || {_, Email} <- ets:tab2list(emails)],
    Fun = fun(Email) ->
                  #{svr_ids:=SvrIds, attachments:=OriAttachments, users:=Users,
                    sender_id:=SenderId} = Email,
                  SvrFun = fun(SvrId) ->
                                   Serverlist = serverlist_lib:get_serverlist(SvrId),
                                   case Serverlist of
                                       undefined ->
                                           [{svr_id, 0}, {name, unicode:characters_to_binary("已删除")}];
                                       _ ->
                                           #{name:=Name} = Serverlist,
                                           [{svr_id, SvrId}, {name, Name}]
                                   end
                           end,
                  Svrs = lists:map(SvrFun, SvrIds),
                  Attachments = [maps:to_list(Attachment) || Attachment <- OriAttachments],
                  UserFun = fun(User) ->
                                    [{user_id, integer_to_list(User)}]
                            end,
                  SenderName = account_lib:get_name(SenderId),
                  NewEmail = Email#{svrs=>Svrs, attachments=>Attachments,
                                    sender_name=>SenderName, users=>lists:map(UserFun, Users)},

                  maps:to_list(NewEmail)
          end,
    Emails = lists:map(Fun, AllEmails),
    {json, [{emails, Emails}]}.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%todo:
format_attachments(AttachmentValStrs) ->
    Fun = fun(AttachmentValStr) ->
                  [ItemTypeOri, ItemTidOri, ItemAmountOri] = string:tokens(AttachmentValStr, "`"),
                  ItemType = list_to_integer(ItemTypeOri),
                  ItemTid = case ItemTidOri of
                               "null" -> 0;
                               _ -> list_to_integer(ItemTidOri)
                           end,
                  ItemAmount = list_to_integer(ItemAmountOri),
                  #{item_type=>ItemType, item_tid=>ItemTid, item_amount=>ItemAmount}
          end,
    lists:map(Fun, AttachmentValStrs).
