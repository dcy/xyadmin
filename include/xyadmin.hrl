-define(YXQ_DOMAIN, "http://youxi.best:8006").

-define(SECRET_STRING, "yxq best").

-define(DB_POOL, dgs_mysql_pool).

-define(CACHE_PROCESS_AMOUNT, 10).

-define(DEBUG_MSG(Str), lager:debug(Str)).
-define(DEBUG_MSG(Format, Args), lager:debug(Format, Args)).
-define(INFO_MSG(Str), lager:info(Str)).
-define(INFO_MSG(Format, Args), lager:info(Format, Args)).
-define(NOTICE_MSG(Str), lager:notice(Str)).
-define(NOTICE_MSG(Format, Args), lager:notice(Format, Args)).
-define(WARNING(Str), lager:warning(Str)).
-define(WARNING_MSG(Format, Args), lager:warning(Format, Args)).
-define(ERROR_MSG(Str), lager:error(Str)).
-define(ERROR_MSG(Format, Args), lager:error(Format, Args)).
-define(CRITICAL_MSG(Str), lager:critical(Str)).
-define(CRITICAL_MSG(Format, Args), lager:critical(Format, Args)).

-define(TRACE_VAR(Arg), io:format("~n******~nModule: ~p, Line: ~p, ~nMy print's ~p is ~p~n******~n", [?MODULE, ?LINE, ??Arg, Arg])).

-define(ASSERT(BoolExpr, Msg), ((fun() ->
                                         case (BoolExpr) of
                                             true -> void;
                                             _V -> erlang:error(Msg)
                                         end
                                 end)())).



%%%%%%%%相关类型定义%%%%%%%%
-define(PLATFORMS, [#{value=>1, name=>"Android", tag=>"android"},
                    #{value=>2, name=>"IOS", tag=>"ios"}]).

-define(SVR_TYPES, [#{value=>1, name=>"QA环境", tag=>"qa"},
                    #{value=>2, name=>"审核环境", tag=>"review"},
                    #{value=>3, name=>"正式环境", tag=>"release"}]).

-define(SVR_STATES, [#{value=>1, name=>"维护中", class=>"badge bg-red"},
                     #{value=>2, name=>"普通", class=>"badge bg-blue"},
                     #{value=>3, name=>"爆满", class=>"badge bg-yellow"},
                     #{value=>4, name=>"新服", class=>"badge bg-green"}
                    ]).

-define(SYSMSG_TYPES, [#{value=>0, name=>"滚动消息"},
                       #{value=>1, name=>"普通消息"}
                      ]).


