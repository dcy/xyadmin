$(document).ready(function() {
    var model = avalon.define({
        $id: "function_model",
        all_serverlists: [],
        serverlists: [],
        serverlists_amount: 0,
        platforms: G_PLATFORMS,
        platform: "",
        channel: "",
        select_locked: false,
        to_edit: function(serverlist) {
            var svr_data = serverlist.$model
            var svr_id = svr_data.svr_id
            store.set('lock_serverlist_' + svr_id, svr_data)
            window.location.href = "/function/lock?svr_id=" + svr_id
        },
        to_lock: function() {
            window.location.href = "/function/lock"
        },
        channels: []
    })

    $(".basic-select2").select2({
        language: "zh-CN"
    })

    function get_channels() {
        $.get("/channel/get_channels", 
              {unixtime: unixtime()},
              function(data) {
                  model.channels = data.channels
              }
             )
    }

    function get_serverlists() {
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function(data) {
                  serverlists = $.map(data.serverlists,format_serverlist_funcs);
                  model.all_serverlists = serverlists;
                  model.serverlists = serverlists;
                  refresh_serverlists_amount();
              }
             )
    }

    function format_serverlist_funcs(serverlist) {
        var func_objs = serverlist.locked_funcs
        var funcs = new Array()
        for (x in func_objs) {
            var item = func_objs[x]
            var func = get_locked_funcs(item.func_id)
            funcs.push(func)
        }
        serverlist.locked_funcs = funcs
        return serverlist
    }

    function refresh_serverlists_amount() {
        model.serverlists_amount = (model.serverlists.length)
    }

    avalon.ready(function() {
        get_channels()
        $('.basic-select2').val("all").trigger("change");
        get_serverlists()

        $('#platform').on("change", function(e) {
            refresh_selected_serverlists()
        })

        $('#channel').on("change", function(e) {
            refresh_selected_serverlists()
        })

        $('input.icheck').on('ifChecked', function(e) {
            console.log("checked")
            model.select_locked = true
            refresh_selected_serverlists()
        })

        $('input.icheck').on('ifUnchecked', function(e) {
            console.log("uncheched")
            model.select_locked = false
            refresh_selected_serverlists()
        })
    })

    function refresh_selected_serverlists() {
        var platform = $('#platform').val()
        var channel = $('#channel').val()
        if (platform == "all" && channel == "all") {
            //todo: has locked
            if (!model.select_locked) {
                model.serverlists = model.all_serverlists
            }
            else {
                var selected_serverlists = new Array()
                for (x in model.all_serverlists.$model) {
                    var item = model.all_serverlists.$model[x]
                    if (is_has_locked_funcs(item)) {
                        selected_serverlists.push(item)
                    }
                }
                model.serverlists = selected_serverlists
            }
            refresh_serverlists_amount()
        }
        else {
            var selected_serverlists = new Array()
            for (x in model.all_serverlists.$model) {
                var item = model.all_serverlists.$model[x]
                if (is_in_platforms(platform, item.platforms)) {
                    if (is_in_channels(channel, item.channels)) {
                        if (model.select_locked) {
                            if (is_has_locked_funcs(item)) {
                                selected_serverlists.push(item)
                            }
                        }
                        else {
                            selected_serverlists.push(item)
                        }
                    }
                }
            }
            model.serverlists = selected_serverlists
        }
        refresh_serverlists_amount()
    }

    function is_in_platforms(platformId, platforms) {
        if (platformId == "all") {
            return true
        }
        else {
            for (x in platforms) {
                if (platforms[x].value == parseInt(platformId)) {
                    return true
                }
            }
            return false
        }
    };

    function is_in_channels(channelId, channels) {
        if (channelId == "all") {
            return true
        }
        else {
            for (x in channels) {
                if (channels[x].id == parseInt(channelId)) {
                    return true
                }
            }
            return false
        }
    };

    function is_has_locked_funcs(serverlist) {
        return serverlist.locked_funcs.length != 0
    }
        

})
