$(document).ready(function() {
    var model = avalon.define({
        $id: "lock_function",
        serverlists: [],
        locked_funcs: [],
        functions: G_FUNCTIONS,
        svr_ids: [],
        to_list: function() {
            window.location.href = "/function/list"
        },
        lock: function() {
            var locked_funcs = $('#locked_funcs').val()
            if (locked_funcs == null) {
                locked_funcs = []
            }
            var svr_ids = $('#svr_ids').val()
            if (svr_ids == null) {
                svr_ids = []
            }
            if (svr_ids.length == 0) {
                vex_alert_error("请选择服务器列表")
                $('button[data-loading-text]').button('reset')
                return
            }
            $.post("/function/lock",
                   {svr_ids: svr_ids, locked_funcs: locked_funcs},
                   function(data) {
                       var svrs = data.fail_svrs
                       var msg = ""
                       if (svrs.length == 0) {
                           msg = "所选全服服务器操作成功"
                       }
                       else {
                           msg = "以下服务器失败: "
                           for (x in svrs) {
                               var svr = svrs[x]
                               msg = msg + "[" + svr.svr_id + ":" + svr.name + "]"
                           }
                       }
                       vex_alert_error(msg)
                       $('button[data-loading-text]').button('reset')
                   }
                  )
        }
    })

    get_serverlists();
    function get_serverlists() {
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function(data) {
                  model.serverlists = data.serverlists
                  var datas = get_url_datas()
                  var svr_id = datas.svr_id
                  if (svr_id != undefined) {
                      $('#svr_ids').val([svr_id]).trigger("change")
                      var serverlist = get_by_key(data.serverlists, "svr_id", svr_id)
                      var locked_funcs_objs = serverlist.locked_funcs
                      var locked_funcs = $.map(locked_funcs_objs, function(obj) {
                          return obj.func_id
                      })
                      $('#locked_funcs').val(locked_funcs).trigger("change")
                  }
              })
    };

    $(".basic-multiple").select2({
        language: "zh-CN"
    });

    var datas = get_url_datas()
    var svr_id = datas.svr_id



})
