$(document).ready(function() {

    $(".basic-select2").select2({
        language: "zh-CN"
    })

    $('.and-switch').bootstrapSwitch({
        onText: "与",
        offText: "或"
    })

    var vm = new Vue({
        el: "#notify",
        data: {
            title: "",
            content: "",
            all_serverlists: [],
            is_pay: false,
            is_and: false,
            is_add_level: false,
            level_type: 0,
            level_value: 1,
            level_infos: [{type:1, name:"等于"}, {type:2, name:"大于"}, {type:3, name:"小于"}],
            serverlists: []
        },
        methods: {
            to_add_level: function(event) {
                this.is_add_level = true
                this.level_value = 1
                this.level_type = 1
            },
            cancel_add_level: function(event) {
                this.is_add_level = false
                this.level_type = 0
                this.level_value = 0

            },
            levelTypeChange: function() {
            },
            to_list: function(event) {
                location.href = "/notify/list"
            }
        }
    })

    get_serverlists()
    function get_serverlists() {
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function(data) {
                  vm.all_serverlists = data.serverlists
              })
    }

    avalon.ready(function() {
        $('#send_notify')
        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                title: {
                    validators: {
                        stringLength: {min:1, max:12},
                        notEmpty: {message: '请输入标题'}
                    }
                },
                content: {
                    validators: {
                        stringLength: {min:1, max:100},
                        notEmpty: {message: '请输入内容'}
                    }
                }
            }

        })
        .on('error.form.bv', function(e) {
            $('button[data-loading-text]').button('reset');
        })
        .on('success.form.bv', function(e) {
            e.preventDefault()
            var serverlists = $('#serverlists').val()
            var is_and = $('#is_and').bootstrapSwitch("state")
            var is_pay = $('#is_pay').bootstrapSwitch("state")
            var level_type = vm.level_type
            if (is_and == true){
                if (level_type == 2 || level_type == 3) {
                    vex_alert_error("条件与的时候，等级条件不能是大于或小于")
                    reset_button()
                    return
                }
            } 
            var serverlist = {title: vm.title, content: vm.content, serverlists: serverlists, is_and: is_and, is_pay: is_pay, level_type: vm.level_type, level_value: vm.level_value}
            $.post("/notify/send",
                   serverlist,
                   function(data) {
                       if (data.result == "success") {
                           vex_confirm("发送成功", function() {
                               location.href = "/notify/list"
                           })
                       }
                       else {
                           vex_confirm("发送失败: " + data.msg, function() {
                           })
                       }
                       reset_button()
                   }
                  )
        })
    })

})
