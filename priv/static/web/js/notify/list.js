$(document).ready(function() {
    var vm = new Vue({
        el: "#notify",
        data: {
            notifys: [],
            notifys_amount: 0
        },
        methods: {
            to_send: function(event) {
                location.href = "/notify/send"
            }
        }
    })
    $.get("/notify/get_notifys",
          {unixtime: unixtime()},
          function(data) {
              vm.notifys = sort_by_key_desc(data.notifys, "id")
              refresh_notifys_amount()
          }
         )

    function refresh_notifys_amount() {
        vm.notifys_amount = vm.notifys.length
    }

})
