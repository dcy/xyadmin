$(document).ready(function() {

    $(".basic-multiple").select2({
        language: "zh-CN"
    })

    $('#start')
    .datetimepicker({
        format: "yyyy-mm-dd hh:ii:00",
        language: "zh-CN",
        todayBtn: true,
        pickerPosition: "bottom-left",
        autoclose: "true"
    })
    //.on('changeDate', function(e) {
    //    $('#add_gift').data('bootstrapValidator').updateStatus('start', 'NOT_VALIDATED', null).validateField('start');
    //});
    $('#deadline')
    .datetimepicker({
        format: "yyyy-mm-dd hh:ii:00",
        language: "zh-CN",
        todayBtn: true,
        pickerPosition: "bottom-left",
        autoclose: "true"
    })
    //.on('changeDate', function(e) {
    //    $('#add_gift').data('bootstrapValidator').updateStatus('deadline', 'NOT_VALIDATED', null).validateField('deadline');
    //});

    var model = avalon.define({
        $id: "activity_model",
        activities: [],
        serverlists: [],
        svr_ids: [],
        start: "",
        deadline: "",
        activity_amount: 0,
        cur_id: "",
        type: "",
        modal_title: "",
        is_running: false,
        to_send_specify_svrs: function(activity) {
            model.type = "specify"
            model.modal_title = "发布到指定服"
            $(this).button("loading")
            $('#svr_ids').val([]).trigger("change")
            model.cur_id = activity.id
            $('#themodal').modal('toggle')
        },
        to_send_all_svrs: function(activity) {
            model.type = "all"
            model.modal_title = "发布到所有服"
            $(this).button("loading")
            model.cur_id = activity.id
            $('#themodal').modal('toggle')
        },
        to_cancel_specify_svrs: function(activity) {
            model.type = "cancel"
            model.modal_title = "取消指定服"
            $(this).button("loading")
            $('#svr_ids').val([]).trigger("change")
            model.cur_id = activity.id
            $('#themodal').modal('toggle')
        },
        //send_specify_svrs: function() {
        //    var svr_ids = $('#svr_ids').val()
        //    if (svr_ids == null) {
        //        svr_ids = []
        //    }
        //    if (svr_ids.length == 0) {
        //        vex_alert_error("请选择服务器列表")
        //        reset_button()
        //        return
        //    }
        //    $.post("/gift/send_specify_svrs",
        //           {sn: model.cur_sn, svr_ids:svr_ids},
        //           handle_send_result
        //          )
        //},
        cancel_specify_svrs: function() {
            var svr_ids = $('#svr_ids').val()
            if (svr_ids == null) {
                svr_ids = []
            }
            if (svr_ids.length == 0) {
                vex_alert_error("请选择服务器列表")
                reset_button()
                return
            }
            model.is_running = true
            $.post("/activity/cancel_specify_svrs",
                   {id: model.cur_id, svr_ids:svr_ids},
                   handle_send_result
                  )
        },
        to_cancel_all_svrs: function(activity) {
            $(this).button("loading")
            vex_confirm("是否取消所有服务器该活动？", function(value) {
                if (value) {
                    $.post("/activity/cancel_all_svrs",
                           {id: activity.id},
                           handle_send_result
                  )
                }
                else{
                    model.is_running = false
                    reset_button()
                }
            })
        },
        send_svrs: function() {
            if (model.start == "") {
                vex_alert_error("请输入开始时间")
                reset_button()
                return
            }
            if (model.deadline == "") {
                vex_alert_error("请输入截止时间")
                reset_button()
                return
            }
            if (model.start >= model.deadline) {
                vex_alert_error("截止时间要大于开始时间")
                reset_button()
                return
            }
            if (model.type == "specify") {
                var svr_ids = $('#svr_ids').val()
                if (svr_ids == null) {
                    svr_ids = []
                }
                if (svr_ids.length == 0) {
                    vex_alert_error("请选择服务器列表")
                    reset_button()
                    return
                }
                model.is_running = true
                $.post("/activity/send_specify_svrs",
                       {id: model.cur_id, start: model.start,
                           deadline:model.deadline, svr_ids:svr_ids},
                       handle_send_result
                      )
            }
            else if (model.type == "all") {
                model.is_running = true
                $.post("/activity/send_all_svrs",
                       {id: model.cur_id, start: model.start,
                           deadline: model.deadline},
                       handle_send_result
                      )
            }
        }

    })

    get_serverlists()
    function get_serverlists(){
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function (data) {
                  model.serverlists = data.serverlists
              })
    }

    get_activities()
    function get_activities() {
        $.get("/activity/get_confs",
              function(data) {
                  model.activities = data.activities
                  refresh_activity_amount()
              })
    }

    function refresh_activity_amount() {
        model.activity_amount = model.activities.length
    }

    $('#themodal').on('hidden.bs.modal', function(e) {
        if (model.is_running == false) {
            reset_button()
        }
    })

    function handle_send_result(data) {
        reset_button()
        model.is_running = false
        var result = data.result
        if (result == "isExpire") {
            vex_alert_error("该活动已经过期")
        }
        else if (result == "success") {
            var svrs = data.fail_svrs
            var msg = ""
            if (svrs.length == 0) {
                msg = "所选全部服务器操作成功"
            }
            else {
                msg = "以下服务器失效： "
                for (x in svrs) {
                    var svr = svrs[x]
                    msg = msg + "[" + svr.svr_id + ":" + svr.name + "]"
                }
            }
            vex_alert_error(msg)
        }
    }
})
