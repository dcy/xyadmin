$(document).ready(function() {
    var model = avalon.define({
        $id: "activity_model",
        all_serverlists: [],
        serverlists: [],
        serverlists_amount: 0,
        platforms: G_PLATFORMS,
        platform: "",
        channel: "",
        activity: {},
        channels: [],
        show_detail: function(e, activity) {
            e.preventDefault()
            $('#themodal').modal("show")
            model.activity = activity
        }
    })

    $(".basic-select2").select2({
        language: "zh-CN"
    })

    function get_channels() {
        $.get("/channel/get_channels", 
              {unixtime: unixtime()},
              function(data) {
                  model.channels = data.channels
              }
             )
    }

    function get_serverlists() {
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function(data) {
                  //serverlists = $.map(data.serverlists,format_serverlist_funcs);
                  serverlists = data.serverlists
                  model.all_serverlists = serverlists
                  model.serverlists = serverlists
                  refresh_serverlists_amount()
              }
             )
    }

    function refresh_serverlists_amount() {
        model.serverlists_amount = (model.serverlists.length)
    }

    avalon.ready(function() {
        get_channels()
        $('.basic-select2').val("all").trigger("change")
        get_serverlists()
        $('#platform').on("change", function(e) {
            refresh_selected_serverlists()
        })

        $('#channel').on("change", function(e) {
            refresh_selected_serverlists()
        })
    })

    function refresh_selected_serverlists() {
        var platform = $('#platform').val()
        var channel = $('#channel').val()
        if (platform == "all" && channel == "all") {
            model.serverlists = model.all_serverlists
        }
        else {
            var selected_serverlists = new Array()
            for (x in model.all_serverlists) {
                var item = model.all_serverlists[x]
                if (is_in_platforms(platform, item.platforms)) {
                    if (is_in_channels(channel, item.channels)) {
                        selected_serverlists.push(item)
                    }
                }
            }
            model.serverlists = selected_serverlists
        }
        refresh_serverlists_amount()
    }

    function is_in_platforms(platformId, platforms) {
        if (platformId == "all") {
            return true
        }
        else {
            for (x in platforms) {
                if (platforms[x].value == parseInt(platformId)) {
                    return true
                }
            }
            return false
        }
    }

    function is_in_channels(channelId, channels) {
        if (channelId == "all") {
            return true
        }
        else {
            for (x in channels) {
                if (channels[x].id == parseInt(channelId)) {
                    return true
                }
            }
            return false
        }
    }



})
