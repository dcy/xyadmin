$(document).ready(function() {
    svr_id = $('#data').data("svr_id");
    var model = avalon.define({
        $id: "edit_serverlist",
        states: G_SVR_STATES,
        types: G_SVR_TYPES,
        platforms: G_PLATFORMS,
        serverlist: {svr_id:"", name:"", clt_vsn: "", area_name:"", state:"", type:"", ip:"",
            local_ip:"", port:"", gm_port:"", log_port:"", platforms:[], channels:[],
        clt_update_url: "", activity_list_id: ""},
        to_list: function() {
            window.location.href = "/serverlist/list";
        },
        channels: []
    });

    get_channels();
    //function get_channels() {
    //    $.get("/channel/get_channels", 
    //          {unixtime: unixtime()},
    //          function(data) {
    //              model.channels = data.channels
    //          }
    //         )
    //};
    function get_channels() {
        $.ajax({
            type: "GET",
            url: "/channel/get_channels",
            async: false,
            success: function(data) {
                model.channels = data.channels
            }
        })
    }

    $(".basic-multiple").select2();

    get_serverlist(svr_id);
    function get_serverlist(svr_id) {
        $.get("/serverlist/get/" + svr_id,
              function(data) {
                  var serverlist_svr = data.serverlist;
                  var serverlist = copy_obj(serverlist_svr);
                  serverlist.state = serverlist_svr.state.value;
                  serverlist.type = serverlist_svr.type.value;
                  var platforms = get_platfroms(serverlist_svr.platforms);
                  serverlist.platforms = get_platfroms(serverlist_svr.platforms);
                  var channels = get_channel_ids(serverlist_svr.channels);
                  model.serverlist = serverlist;
                  $('.basic-multiple').val(channels).trigger("change");
              }
             )
    };

    function get_platfroms(platform_objs) {
        var platforms = new Array();
        for (x in platform_objs){
            platforms.push(platform_objs[x].value.toString());
        };
        return platforms;
    };

    function get_channel_ids(channel_objs) {
        var channels = new Array();
        for (x in channel_objs) {
            channels.push(channel_objs[x].id.toString());
        };
        return channels;
    };

    avalon.ready(function() {
        $('#edit_serverlist').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                svr_id: {
                    validators: {
                        notEmpty: {message: '请输入服务器id'}
                    }
                },
                name: {
                    message: '服务器名无效',
                    validators: {
                        notEmpty: {message: '请输入服务器名'},
                        stringLength: {min: 1, max: 20},
                        regexp: {regexp: "^[\u4e00-\u9fa5a-zA-Z0-9\-]+$", 
                            message: '只能是中文，字母，数字, -'}
                    }
                },
                clt_vsn: {
                    validators: {
                        notEmpty: {message: '请输入客户端版本号'},
                        regexp: {regexp: "^([0-9]+)[.]([0-9]+)[.]([0-9]+)$", 
                            message: '版本号非法'}
                    }
                },
                area_name: {
                    validators: {
                        notEmpty: {message: '请输入大区名称'},
                        stringLength: {min:1, max:16}
                    }
                },
                state: {
                    validators: {
                        notEmpty: {message: '请选择状态'}
                    }
                },
                type: {
                    validators: {
                        notEmpty: {message: '请选择类型'}
                    }
                },
                ip: {
                    validators: {
                        notEmpty: {message: '请填写ip'},
                        regexp: {regexp: "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", message: 'ip非法'}
                    }
                },
                local_ip: {
                    validators: {
                        regexp: {regexp: "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", message: 'ip非法'}
                    }
                },
                port: {
                    validators: {
                        stringLength: {min:4, max:5},
                        notEmpty: {message: '请输入游戏端口'}
                    }
                },
                gm_port: {
                    validators: {
                        stringLength: {min:4, max:5},
                        notEmpty: {message: '请输入GM端口'}
                    }
                },
                log_port: {
                    validators: {
                        stringLength: {min:4, max:5},
                        notEmpty: {message: '请输入日志端口'}
                    }
                },
                platforms: {
                    validators: {
                        notEmpty: {message: '请选择平台'}
                    }
                },
                channels: {
                    validators: {
                        notEmpty: {message: '请选择渠道'}
                    }
                },
                clt_update_url: {
                    validators: {
                        uri: {message: '请输入地址'}
                    }
                },
                activity_list_id: {
                    validators: {
                        notEmpty: {message: '请输入活动列表id'},
                        regexp: {regexp: "^[0-9]+$", message: '必须为数字'}
                    }
                }
            }

        })
        .on('error.form.bv', function(e) {
            $('button[data-loading-text]').button('reset');
        })
        .on('success.form.bv', function(e) {
            e.preventDefault();
            var serverlist_vals = model.serverlist.$model;
            serverlist_vals.channels = $('#channels').val();
            $.post("/serverlist/edit",
                   serverlist_vals,
                   function(data) {
                       if (data.result == "success") {
                           msg_sth("成功修改");
                           vex_confirm("返回列表", function() {
                               window.location.href = "/serverlist/list";
                           });
                       }
                       else if (data.result == "svrIdNotExist") {
                           vex_alert_error("该服务器已不存在！");
                       };
                       $('button[data-loading-text]').button('reset');
                   })
        })
    });



});
