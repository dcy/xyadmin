$(document).ready(function() {
    var model = avalon.define({
        $id: "serverlist_model",
        all_serverlists: [],
        fuck: "fuck",
        serverlists: [],
        serverlists_amount: 0,
        platforms: G_PLATFORMS,
        platform: "",
        channel: "",
        channels: [],
        serverlist: {svr_id:"", name:"", clt_vsn: "", area_name:"", state:"", type:"", ip:"",
            local_ip:"", port:"", gm_port:"", log_port:"", platforms:[], channels:[],
        clt_update_url: "", activity_list_id: ""},
        to_add: function() {
            window.location.href = "/serverlist/add"
        },
        show_detail: function(serverlist) {
            model.serverlist = serverlist;
            $('#themodal').modal('toggle');
        },
        to_edit: function(serverlist) {
            window.location.href = "/serverlist/edit/" + serverlist.svr_id;
        },
        del_serverlist: function(serverlist, remove_fun) {
            vex_confirm("确定删除服务器列表 "+serverlist.svr_id+"~"+serverlist.name+" 吗?", function(value) {
                if (value) {
                    $.post("/serverlist/del",
                           {svr_id: serverlist.svr_id},
                           function(data) {
                               if (data.result == "success") {
                                   msg_sth("成功删除服务器列表 "+serverlist.svr_id+"~"+serverlist.name);
                                   remove_fun();
                                   var new_serverlists = new Array();
                                   for (x in model.all_serverlists.$model) {
                                       var item = model.all_serverlists.$model[x];
                                       if (item.svr_id != serverlist.svr_id) {
                                           new_serverlists.push(item);
                                       }
                                   };
                                   model.all_serverlists = new_serverlists;
                                   refresh_serverlists_amount();
                               }
                           });
                }
            })
        }
    });
    $(".basic-select2").select2({
        language: "zh-CN"
    });

    avalon.ready(function() {
        get_channels();
        $('.basic-select2').val("all").trigger("change");
        get_serverlists();
        $('#platform').on("change", function(e) {
            refresh_selected_serverlists();
        });

        $('#channel').on("change", function(e) {
            refresh_selected_serverlists();
        });
    });

    function get_channels() {
        $.get("/channel/get_channels", 
              {unixtime: unixtime()},
              function(data) {
                  model.channels = data.channels
              }
             )
    };


    function refresh_serverlists_amount() {
        model.serverlists_amount = (model.serverlists.length)
    };

    function get_serverlists() {
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function(data) {
                  serverlists = data.serverlists;
                  model.all_serverlists = serverlists;
                  model.serverlists = serverlists;
                  refresh_serverlists_amount();
              }
             )
    };


    function refresh_selected_serverlists() {
        var platform = $('#platform').val();
        var channel = $('#channel').val();
        if (platform == "all" && channel == "all") {
            model.serverlists = model.all_serverlists;
        }
        else {
            var selected_serverlists = new Array();
            for (x in model.all_serverlists) {
                var item = model.all_serverlists[x];
                if (is_in_platforms(platform, item.platforms)) {
                    if (is_in_channels(channel, item.channels)) {
                        selected_serverlists.push(item);
                    }
                }
            };
            model.serverlists = selected_serverlists;
        }
        refresh_serverlists_amount();
    };

    function is_in_platforms(platformId, platforms) {
        if (platformId == "all") {
            return true
        }
        else {
            for (x in platforms) {
                if (platforms[x].value == parseInt(platformId)) {
                    return true
                }
            }
            return false
        }
    };

    function is_in_channels(channelId, channels) {
        if (channelId == "all") {
            return true
        }
        else {
            for (x in channels) {
                if (channels[x].id == parseInt(channelId)) {
                    return true
                }
            }
            return false
        }
    };
})
