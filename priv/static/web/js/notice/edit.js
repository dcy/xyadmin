$(document).ready(function() {
    var edit_notice = store.get("edit_notice")
    var type = store.get("edit_notice_type")
    $('#summernote').summernote({
        height: 300,                 // set editor height

        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor

        focus: true,                 // set focus to editable area after initializing summernote
    });

    var model = avalon.define({
        $id: "edit_notice",
        form_title: "编辑",
        platforms: G_PLATFORMS,
        to_list: function() {
            window.location.href = "/notice/list/" + type
        },
        notice: {id: "", vsn: "", platforms: [], content: ""}
    })
    var platforms = $.map(edit_notice.platforms, function(platform) {
        return platform.value.toString()
    })
    edit_notice.platforms = platforms
    model.notice = edit_notice
    $('#summernote').code(edit_notice.content)

    avalon.ready(function() {
        $('#add_notice').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                vsn: {
                    validators: {
                        regexp: {regexp: "^[0-9.]+$", message: '版本号非法'},
                        notEmpty: {message: '请输入版本号'}
                    }
                },
                platforms: {
                    validators: {
                        notEmpty: {message: '请选择所属平台'}
                    }
                },
                content: {
                    validators: {
                        notEmpty: {message: '请输入内容'}
                    }
                }
            }

        })
        .on('error.form.bv', function(e) {
            $('button[data-loading-text]').button('reset');
        })
        .on('success.form.bv', function(e) {
            e.preventDefault()
            var notice_vals = model.notice.$model
            var content = $('#summernote').code()
            notice_vals.content = content
            console.log("notice_vals", notice_vals)
            $.post("/notice/edit/" + type,
                   notice_vals,
                   function(data) {
                       if (data.result == "success") {
                           msg_sth("成功添加")
                           vex_confirm("返回列表", function(value) {
                                   window.location.href = "/notice/list/" + type
                           })
                       }
                       $('button[data-loading-text]').button('reset');
                   })
        })

    });

    
})
