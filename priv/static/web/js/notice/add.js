$(document).ready(function() {
    var type = $('#data').data("type");
    console.log("type: ", type);
    $('#summernote').summernote({
        height: 300,                 // set editor height

        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor

        focus: true,                 // set focus to editable area after initializing summernote
    });

    var model = avalon.define({
        $id: "add_notice",
        form_title: "添加",
        platforms: G_PLATFORMS,
        cancel: handle_cancel_add,
        to_list: function() {
            window.location.href = "/notice/list/" + type
        },
        notice: {vsn: "", platforms: [], content: ""}
    });


    $('#to_list').click(function() {
        window.location.href ="/notice/list/" + type;
    });

    function handle_cancel_add() {
        model.notice.vsn = "";
        model.notice.platforms = [];
        model.notice.content = "";
        $('#add_notice').bootstrapValidator('revalidateField', 'vsn')
        .bootstrapValidator('revalidateField', 'platforms')
        .bootstrapValidator('revalidateField', 'content');
    };

    avalon.ready(function() {
        $('#add_notice').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                vsn: {
                    validators: {
                        regexp: {regexp: "^[0-9.]+$", message: '版本号非法'},
                        notEmpty: {message: '请输入版本号'}
                    }
                },
                platforms: {
                    validators: {
                        notEmpty: {message: '请选择所属平台'}
                    }
                },
                content: {
                    validators: {
                        notEmpty: {message: '请输入内容'}
                    }
                }
            }

        })
        .on('error.form.bv', function(e) {
            $('button[data-loading-text]').button('reset');
        })
        .on('success.form.bv', function(e) {
            e.preventDefault();
            var notice_vals = model.notice.$model;
            var content = $('#summernote').code();
            notice_vals.content = content;
            $.post("/notice/add/" + type,
                   notice_vals,
                   function(data) {
                       if (data.result == "success") {
                           msg_sth("成功添加");
                           vex_confirm("返回列表", function(value) {
                               if (value){
                                   window.location.href = "/notice/list/" + type;
                               }
                               else {
                                   handle_cancel_add();
                               };
                           });
                       }
                       $('button[data-loading-text]').button('reset');
                   })
        })

    });









});
