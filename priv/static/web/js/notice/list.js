$(document).ready(function() {
    var type = $('#data').data("type");

    var model = avalon.define({
        $id: "notice_model",
        notices: [],
        notices_amount: 0,
        to_edit: function(notice) {
            store.set("edit_notice", notice.$model)
            store.set("edit_notice_type", type)
            window.location.href = "/notice/edit/" + type
        },
        to_add: function() {
            window.location.href = "/notice/add/" + type;
        },
        del_notice: function(notice, remove_fun) {
            vex_confirm("确定删除公告 " + notice.vsn + " 吗？", function(value) {
                if (value) {
                    $.post("/notice/del",
                           {id: notice.id},
                           function(data) {
                               if (data.result == "success") {
                                   msg_sth("成功删除公告 ", notice.vsn);
                                   remove_fun();
                                   refresh_notices_amount();
                               }
                           })
                }
            })
        }
    });

    function refresh_notices_amount() {
        model.notices_amount = model.notices.length
    };

    get_notices();
    function get_notices() {
        $.get("/notice/get_notices/" + type,
              {unixtime: unixtime()},
              function(data) {
                  console.log(data.notices);
                  model.notices = sort_by_key_desc(data.notices, "vsn");
                  refresh_notices_amount();
              })
    };

});
