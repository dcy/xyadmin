$(document).ready(function() {
    var login_model = avalon.define({
        $id: "login",
        username: "",
        password: "",
        do_login: function(e) {
            e.preventDefault();
            if (login_model.username.length == 0) {
                vex_alert_error("请输入用户名");
                return;
            };
            if(login_model.password.length == 0) {
                vex_alert_error("请输入密码");
                return;
            };
            $.post("/account/login",
                   {username: login_model.username, password: login_model.password},
                   function(data) {
                       console.log("result", data);
                       result = data.result;
                       if (result == "user_not_exist") {
                           vex_alert_error("该用户不存在");
                           return;
                       }
                       else if (result == "password_wrong") {
                           vex_alert_error("密码错误");
                           return;
                       }
                       else {
                           msg_sth("登录成功");
                           setTimeout(function() {
                               window.location.href = "/"
                           }, 1000);
                       }
                   }
                  );
        }
    })
})
