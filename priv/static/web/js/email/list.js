$(document).ready(function() {
    var type = $('#data').data("type")
    console.log("type", type)
    var model = avalon.define({
        $id: "email_model",
        emails: [],
        emails_amount: 0,
        send_person: function() {
            window.location.href = "/email/send/person"
        },
        send_svr: function() {
            window.location.href = "/email/send/svr"
        },
        del_email: function(email, remove_fun) {
            vex_confirm("确定删除邮件'" + email.title + "'?", function(value) {
                if (value) {
                    $.post("/email/del",
                           {id: email.id},
                           function(data) {
                               if (data.result == "success") {
                                   msg_sth("成功删除邮件'" + email.title + "'")
                                   remove_fun()
                                   refresh_emails_amount()
                               }
                           }
                          )
                }
            }) 
        }
        
    })

    function refresh_emails_amount() {
        model.emails_amount = model.emails.length
    }

    get_emails();
    function get_emails() {
        $.get("/email/get_emails",
              {unixtime: unixtime()},
              function(data) {
                  model.emails = sort_by_key_desc(data.emails, "id")
                  refresh_emails_amount()
              })
    }


});
