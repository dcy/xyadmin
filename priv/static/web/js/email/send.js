$(document).ready(function() {
    var type = $('#data').data("type");
    var empty_attachment = {item_type: 0, item_tid: "", item_infos:[], item_amount: ""};
    var model = avalon.define({
        $id: "send_email",
        attachments: [],
        item_types: G_ITEM_TYPES,
        type: type,
        to_list: function() {
            window.location.href = "/email/list"
        },
        item_type_change: function(e, attachment) {
            attachment.item_tid = ""
            attachment.item_infos = []
            var item_type = get_item_type(e.target.value)
            var conf = item_type.conf
            if (conf != undefined) {
                $.get("/gift/get_data_conf/" + conf,
                       {unixtime: unixtime()},
                       function(data) {
                           attachment.item_infos = data.item_infos
                       }
                      )
            }
        },
        to_add_attachment: function() {
            model.attachments.push(copy_obj(empty_attachment));
        },
        serverlists: [],
        email: {title: "", serverlists: [], users: "", content: ""}
    });

    get_serverlists();
    function get_serverlists() {
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function(data) {
                  model.serverlists = data.serverlists
              })
    };
    if (type == "svr"){
        $(".basic-select2").select2({
            language: "zh-CN"
        });
    }
    else {
        $(".basic-select2").select2({
            language: "zh-CN",
            maximumSelectionLength: 1
        });
    };
    $("input[name='users']").tagsinput();

    avalon.ready(function() {
        $('#send_email')
        .find('[name="users"]').change(function(e) {
            $('#send_email').bootstrapValidator('revalidateField', 'users');
        }).end()
        .bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                title: {
                    validators: {
                        stringLength: {min:1, max:12},
                        notEmpty: {message: '请输入邮件标题'}
                    }
                },
                serverlists: {
                    validators: {
                        notEmpty: {message: '请选择服务器'}
                    }
                },
                users: {
                    validators: {
                        regexp: {regexp: "^[0-9\,]+$", message: '只能是数字和","'}
                    }
                },
                content: {
                    validators: {
                        stringLength: {min:1, max:1000},
                        notEmpty: {message: '请输入内容'}
                    }
                }
            }

        })
        .on('error.form.bv', function(e) {
            $('button[data-loading-text]').button('reset');
        })
        .on('success.form.bv', function(e) {
            e.preventDefault();
            var email_vals = model.email.$model;
            email_vals.serverlists = $('#serverlists').val();
            //todo
            var attachments = format_attachments(model.attachments.$model);
            if (attachments == "error") {
                $('button[data-loading-text]').button('reset');
                return false;
            };
            email_vals.attachments = attachments;
            $.post("/email/send/" + type,
                   email_vals,
                   function(data) {
                       var svrs = data.fail_svrs
                       var msg = ""
                       if (svrs.length == 0) {
                           msg = "所选服务器全部发送成功"
                       }
                       else {
                           msg = "以下服务器失败: "
                           for (x in svrs) {
                               var svr = svrs[x]
                               msg = msg + "[" + svr.svr_id + ":" + svr.name + "]"
                           }
                       }
                       reset_button()
                       vex_confirm(msg, function(){
                           window.location.href = "/email/list"
                       })
                   })
        })

    });

    //todo:
    function format_attachment(attachment) {
        return format_element(attachment.item_type) + "`" + format_element(attachment.item_tid) + "`" + format_element(attachment.item_amount)
    };

    function format_attachments(attachments) {
        var new_items = new Array();
        for (x in attachments) {
            var attachment = attachments[x];
            if (attachment.item_type != 0) {
                var type_obj = get_item_type(attachment.item_type);
                if (attachment.item_amount == "") {
                    vex_alert_error("附件 " + type_obj.name + " 的数量不能为空");
                    return "error"
                }
                if (type_obj.need_item_id && attachment.item_tid == "") {
                    vex_alert_error("附件 " + type_obj.name + " 的tid不能为空");
                    return "error"
                }
                new_items.push(format_attachment(attachment))
            }
        }
        return new_items
    };

    function format_element(ele) {
        if (ele == "") {
            return null
        }
        else {
            return ele
        }
    }


});
