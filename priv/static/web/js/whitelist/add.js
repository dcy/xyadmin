$(document).ready(function() {
    var model = avalon.define({
        $id: "add_whitelist",
        whitelist: {account_id: "", username: "", is_advanced: true}, 
        cancel: handle_cancel_add,
        to_list: function() {
            window.location.href = "/whitelist/list/account"
        },
        added_whitelists: [],
        added_whitelists_amount: 0
    });

    function handle_cancel_add() {
        model.whitelist.account_id = "";
        model.whitelist.username = "";
        $('#add_whitelist').bootstrapValidator('revalidateField', 'account_id')
        .bootstrapValidator('revalidateField', 'username');
    };

    $('.form-horizontal').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        excluded: ':disabled',
        fields: {
            account_id: {
                validators: {
                    notEmpty: {message: '请输入用户id'},
                    stringLength: {min: 1, max: 60},
                    regexp: {regexp: "^[a-zA-Z0-9]+$", 
                        message: '只能是字母，数字'}
                }
            },
            username: {
                validators: {
                    notEmpty: {message: '请输入用户名'}
                }
            }
        }
    })
    .on('success.form.bv', function(e) {
        var is_advanced = $('#is_advanced').bootstrapSwitch("state");
        e.preventDefault();
        added_whitelist = copy_obj(model.whitelist.$model);
        added_whitelist.is_advanced = is_advanced;
        $.post("/whitelist/add",
               added_whitelist,
               function(data) {
                   var result = data.result;
                   if (result == "success") {
                       msg_sth("成功添加一个白名单: " + added_whitelist.account_id + "~" + added_whitelist.username);
                       handle_cancel_add();
                       model.added_whitelists.unshift(added_whitelist);
                       model.added_whitelists_amount = model.added_whitelists_amount + 1;
                   }
                   else {
                       vex_alert_error("请检查新加渠道的字段是否和已有的重复");
                   };
                   $('button[data-loading-text]').button('reset');
               }
              );
    })
    .on('error.form.bv', function(e) {
        e.preventDefault();
        $('button[data-loading-text]').button('reset');
    });

});
