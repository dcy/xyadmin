$(document).ready(function() {
    var type = $('#data').data("type");

    var model = avalon.define({
        $id: "whitelist_model",
        whitelists: [],
        whitelists_amount: 0,
        whitelist: {id: "", account_id: "", username: "", is_advanced: true}, 
        to_add: function() {
            if (type == "account") {
                window.location.href = "/whitelist/add"
            }
            else {
                window.location.href = "/whitelist/add_udid"
            }
        },
        to_edit: function(whitelist) {
            model.whitelist = whitelist.$model;
            $('#is_advanced').bootstrapSwitch('state', model.whitelist.is_advanced);
            $('#themodal').modal('show');
        },
        del_whitelist: function(whitelist, remove_fun) {
            if (type == "account") {
                vex_confirm("确定删除 " + whitelist.username + " 吗?", function(value) {
                    if (value) {
                        $.post("/whitelist/del",
                               {id: whitelist.id},
                               function(data) {
                                   if (data.result == "success") {
                                       msg_sth("成功删除白名单 " + whitelist.username);
                                       remove_fun();
                                       refresh_whitelists_amount();
                                   }
                               }
                              )
                    }
                })
            }
            else {
                vex_confirm("确定删除 " + whitelist.udid + " 吗?", function(value) {
                    if (value) {
                        $.post("/whitelist/del_udid",
                               {id: whitelist.id},
                               function(data) {
                                   if (data.result == "success") {
                                       msg_sth("成功删除白名单 " + whitelist.udid);
                                       remove_fun();
                                       refresh_whitelists_amount();
                                   }
                               }
                              )
                    }
                })
            }
        }
    });

    function refresh_whitelists_amount() {
        model.whitelists_amount = model.whitelists.length;
    };

    get_whitelists();

    function get_whitelists() {
        $.get("/whitelist/get_whitelists/" + type,
              {unixtime: unixtime()},
              function(data) {
                  model.whitelists = data.whitelists;
                  refresh_whitelists_amount();
              }
             )
    };

    $('.form-horizontal').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        excluded: ':disabled',
        fields: {
            account_id: {
                validators: {
                    notEmpty: {message: '请输入用户id'},
                    stringLength: {min: 1, max: 60},
                    regexp: {regexp: "^[a-zA-Z0-9]+$", 
                        message: '只能是字母，数字'}
                }
            },
            username: {
                validators: {
                    notEmpty: {message: '请输入用户名'}
                }
            }
        }
    })
    .on('success.form.bv', function(e) {
        var is_advanced = $('#is_advanced').bootstrapSwitch("state");
        e.preventDefault();
        added_whitelist = copy_obj(model.whitelist.$model);
        added_whitelist.is_advanced = is_advanced;
        $.post("/whitelist/update",
               added_whitelist,
               function(data) {
                   var result = data.result;
                   if (result == "success") {
                       msg_sth("成功修改");
                       vex_confirm("成功修改", function() {
                           window.location.reload();
                       })
                   }
                   else {
                       vex_alert_error("请检查新加渠道的字段是否和已有的重复");
                   };
                   $('button[data-loading-text]').button('reset');
               }
              );
    })
    .on('error.form.bv', function(e) {
        e.preventDefault();
        $('button[data-loading-text]').button('reset');
    });

});
