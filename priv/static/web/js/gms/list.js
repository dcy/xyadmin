$(document).ready(function() {
    $(".basic-select2").select2({
        language: "zh-CN"
    });

    var vm = new Vue({
        el: "#gms",
        data: {
            serverlists: [],
            svr_ids: [],
            type: "open"
        },
        methods: {
            closeModal: function() {
                reset_button()
            },
            openGuildDungeon: function(event) {
                vm.type = "open"
                event.preventDefault()
                $('#themodal').modal('toggle')
                //$.get("/gms/open_guild_dungeon",
                //      {unixtime: unixtime()},
                //      function(data) {
                //          if (data.result == "success") {
                //              vex_confirm("成功开启", function() {
                //                  reset_button()
                //              })
                //          }
                //      }
                //     )
            },
            closeGuildDungeon: function(event) {
                event.preventDefault()
                vm.type = "close"
                $('#themodal').modal('toggle')

                //$.get("/gms/close_guild_dungeon",
                //      {unixtime: unixtime()},
                //      function(data) {
                //          if (data.result == "success") {
                //              vex_confirm("成功关闭", function(){
                //                  reset_button()
                //              })
                //          }
                //      }
                //     )
            },
            handleGuildDungeon: function() {
                var url = ""
                if (vm.type == "open") {
                    url = "/gms/open_guild_dungeon"
                }
                else {
                    url = "/gms/close_guild_dungeon"
                }
                var svr_ids = $('#svr_ids').val()

                if (svr_ids == null) {
                    svr_ids = []
                }
                if (svr_ids.length == 0) {
                    vex_alert_error("请选择服务器列表")
                    reset_button()
                    return
                }
                $.post(url, {svr_ids: svr_ids}, handle_guild_dungeon_result)
            }
        }
    })

    function handle_guild_dungeon_result(data) {
        reset_button()
        var svrs = data.fail_svrs
        var msg = ""
        if (svrs.length == 0) {
            msg = "所选全部服务器操作成功"
        }
        else {
            msg = "以下服务器失效： "
            for (x in svrs) {
                var svr = svrs[x]
                msg = msg + "[" + svr.svr_id + ":" + svr.name + "]"
            }
        }
        vex_alert_error(msg)
    }


    get_serverlists()
    function get_serverlists() {
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function(data) {
                  vm.serverlists = data.serverlists
              })
    }
})
