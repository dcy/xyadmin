$(document).ready(function() {

    $('#start')
    .datetimepicker({
        format: "yyyy-mm-dd hh:ii:00",
        language: "zh-CN",
        todayBtn: true,
        pickerPosition: "bottom-left",
        autoclose: "true"
    })
    .on('changeDate', function(e) {
        $('#add_gift').data('bootstrapValidator').updateStatus('start', 'NOT_VALIDATED', null).validateField('start');
    });
    $('#deadline')
    .datetimepicker({
        format: "yyyy-mm-dd hh:ii:00",
        language: "zh-CN",
        todayBtn: true,
        pickerPosition: "bottom-left",
        autoclose: "true"
    })
    .on('changeDate', function(e) {
        $('#add_gift').data('bootstrapValidator').updateStatus('deadline', 'NOT_VALIDATED', null).validateField('deadline');
    });

    var type = $('#data').data("type")
    var empty_item = {item_type: 0, item_tid: "", item_infos:[], item_amount: ""}
    var model = avalon.define({
        $id: "add_gift",
        items: [],
        item_types: G_ITEM_TYPES,
        type: type,
        gift: {sn:"", code:"", amount:"", start:"", deadline:"", email_title:"", email_content:"",
            remark1:"", remark2:""},
        to_list: function() {
            window.location.href = "/gift/list/" + type
        },
        item_type_change: function(e, item) {
            item.item_tid = ""
            item.item_infos = []
            var item_type = get_item_type(e.target.value)
            var conf = item_type.conf
            if (conf != undefined) {
                $.get("/gift/get_data_conf/" + conf,
                       {unixtime: unixtime()},
                       function(data) {
                           item.item_infos = data.item_infos
                       }
                      )
            }
        },
        to_add_item: function() {
            model.items.push(copy_obj(empty_item))
        }
    })



    avalon.ready(function() {
        $('#add_gift').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                sn: {
                    validators: {
                        notEmpty: {message: '请输入编号'},
                        regexp: {regexp: "^[a-zA-Z0-9]+$", 
                            message: '只能是字母，数字'}
                    }
                },
                code: {
                    validators: {
                        stringLength: {min:3, max:20},
                        regexp: {regexp: "^[a-zA-Z0-9]+$", 
                            message: '只能是字母，数字'},
                        notEmpty: {message: '请输入固定码'}
                    }
                },
                start: {
                    validators: {
                        notEmpty: {message: '请输入开始时间'}
                    }
                },
                deadline: {
                    validators: {
                        notEmpty: {message: '请输入截止时间'}
                    }
                },
                items: {
                    validators: {
                        notEmpty: {message: '请选择礼品'}
                    }
                },
                amount: {
                    validators: {
                        notEmpty: {message: '请输入数量'}
                    }
                },
                email_title: {
                    validators: {
                        stringLength: {min:3, max:15},
                        notEmpty: {message: '请输入邮件标题'}
                    }
                },
                email_content: {
                    validators: {
                        stringLength: {min:3, max:600},
                        notEmpty: {message: '请输入邮件内容'}
                    }
                },
                content: {
                    validators: {
                        stringLength: {min:1, max:160},
                        notEmpty: {message: '请输入内容'}
                    }
                }
            }

        })
        .on('error.form.bv', function(e) {
            $('button[data-loading-text]').button('reset');
        })
        .on('success.form.bv', function(e) {
            e.preventDefault()
            var gift_vals = model.gift.$model

            if (model.items.$model.length == 0) {
                vex_alert_error("礼品列表不能为空")
                reset_button()
                return false
            }
            if (model.gift.deadline <= model.gift.start) {
                vex_alert_error("截止时间要大于开始时间")
                reset_button()
                return false
            }
            if (parseInt(model.gift.amount) <= 0) {
                vex_alert_error("数量不能小于等于0")
                reset_button()
                return false
            }
            if (parseInt(model.gift.amount) > 25000) {
                vex_alert_error("数量不能大于25000")
                reset_button()
                return false
            }
            var items = format_attachments(model.items.$model)
            if (items == "error") {
                $('button[data-loading-text]').button('reset')
                return false
            }

            gift_vals.items = items
            $.post("/gift/add/" + type,
                   gift_vals,
                   function(data) {
                       var result = data.result
                       if (result == "success") {
                           vex_confirm("成功添加", function() {
                               window.location.href = "/gift/list/" + type
                           })
                       }
                       else if(result == "snExist"){
                           vex_alert_error("同样编号Sn的礼包码已经存在")
                       }
                       else if(result == "codeExist") {
                           vex_alert_error("同样固定码的礼包已经存在")
                       }
                       reset_button()
                   }
                  )

        })
    })

    function format_attachment(attachment) {
        return format_element(attachment.item_type) + "`" + format_element(attachment.item_tid) + "`" + format_element(attachment.item_amount)
    }

    function format_attachments(attachments) {
        var new_items = new Array();
        for (x in attachments) {
            var attachment = attachments[x];
            if (attachment.item_type != 0) {
                var type_obj = get_item_type(attachment.item_type);
                if (attachment.item_amount == "") {
                    vex_alert_error("附件 " + type_obj.name + " 的数量不能为空");
                    return "error"
                }
                if (type_obj.need_item_id && attachment.item_tid == "") {
                    vex_alert_error("附件 " + type_obj.name + " 的tid不能为空");
                    return "error"
                }
                new_items.push(format_attachment(attachment))
            }
        }
        return new_items
    }

    function format_element(ele) {
        if (ele == "") {
            return null
        }
        else {
            return ele
        }
    }
})
