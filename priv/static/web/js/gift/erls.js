$(document).ready(function() {
    var model = avalon.define({
        $id: "gift_model",
        to_refresh: function() {
            $.post("/gift/refresh_erlang_files",
                   {unixtime: unixtime()},
                   function(data) {
                       vex_confirm("重新生成成功, 可下载新文件替换", function() {})
                       reset_button()
                   }
                  )
        }
    })
})
