$(document).ready(function() {
    var type = $('#data').data("type")

    $('button[data-loading-text]').click(function () {
        console.log("here");
        var btn = $(this).button('loading');
        //setTimeout(function () {
        //    btn.button('reset');
        //}, 3000);
    });
    
    $(".basic-multiple").select2({
        language: "zh-CN"
    });

    var model = avalon.define({
        $id: "gift_model",
        serverlists: [],
        svr_ids: [],
        cur_sn: "",
        gifts: [],
        gifts_amount: 0,
        to_send_specify_svrs: function(gift) {
            $(this).button("loading")
            $('#svr_ids').val([]).trigger("change")
            model.cur_sn = gift.sn
            $('#themodal').modal('toggle')
        },
        close_modal: function() {
            reset_button()
        },
        send_specify_svrs: function() {
            var svr_ids = $('#svr_ids').val()
            if (svr_ids == null) {
                svr_ids = []
            }
            if (svr_ids.length == 0) {
                vex_alert_error("请选择服务器列表")
                reset_button()
                return
            }
            $.post("/gift/send_specify_svrs",
                   {sn: model.cur_sn, svr_ids:svr_ids},
                   handle_send_result
                  )
        },
        to_send_svrs: function(gift) {
            $(this).button("loading")
            vex_confirm("确定要发布到所有服务器吗？", function(value) {
                if (value) {
                    $.post("/gift/send_svrs",
                           {sn: gift.sn},
                           handle_send_result
                          )
                }
                else {
                    reset_button()
                }
            })
        },
        to_del: function(gift, remove_fun) {
            $(this).button("loading")
            vex_confirm("确定删除该礼包码?", function(value) {
                if (value) {
                    $.post("/gift/del",
                           {sn: gift.sn},
                           function(data) {
                               if (data.result == "success") {
                                   vex_confirm("删除成功", function(){
                                       remove_fun()
                                   })
                               }
                           }
                          )
                }
                else {
                }
                reset_button()
            })
        },
        to_add: function() {
            window.location.href = "/gift/add/" + type
        }
    })

    get_serverlists()
    function get_serverlists(){
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function (data) {
                  model.serverlists = data.serverlists
              })
    }


    function refresh_gifts_amount() {
        model.gifts_amount = model.gifts.length
    }

    get_gifts()
    function get_gifts() {
        $.get("/gift/get_gifts/" + type,
              {unixtime: unixtime()},
              function(data) {
                  var gifts = $.map(data.gifts, format_gift)
                  model.gifts = sort_by_key_desc(gifts, "sn")
                  refresh_gifts_amount()
              }
             )
    }

    function format_gift(gift) {
        var items = $.map(gift.items, format_item)
        gift.items = items
        return gift
    }

    function format_item(item) {
        var item_obj = get_by_key(G_ITEM_TYPES, "type_id", item.item_type)
        item.item_type_name = item_obj.name
        return item
    }

    function handle_send_result(data) {
        reset_button()
        var result = data.result
        if (result == "isExpire") {
            vex_alert_error("该礼包码已经过期")
        }
        else if (result == "success") {
            var svrs = data.fail_svrs
            var msg = ""
            if (svrs.length == 0) {
                msg = "所选全部服务器操作成功"
            }
            else {
                msg = "以下服务器失效： "
                for (x in svrs) {
                    var svr = svrs[x]
                    msg = msg + "[" + svr.svr_id + ":" + svr.name + "]"
                }
            }
            vex_alert_error(msg)
        }
    }


})
