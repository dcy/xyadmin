$(document).ready(function() {
    var model = avalon.define({
        $id: "add_channel",
        channel: {name: "", tag:""},
        cancel: handle_cancel_add,
        to_list: function() {
            window.location.href = "/channel/list"
        },
        added_channels: [],
        added_channels_amount: 0
    });

    function handle_cancel_add() {
            model.channel.name = "";
            model.channel.tag = "";
            $('#add_channel').bootstrapValidator('revalidateField', 'name')
            .bootstrapValidator('revalidateField', 'tag');
    };

    $('#add_channel').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        excluded: ':disabled',
        fields: {
            name: {
                message: '服务器名无效',
                validators: {
                    notEmpty: {message: '请输入服务器名'},
                    stringLength: {min: 1, max: 10},
                    regexp: {regexp: "^[\u4e00-\u9fa5a-zA-Z0-9]+$", 
                        message: '只能是中文，字母，数字'}
                }
            },
            tag: {
                validators: {
                    notEmpty: {message: '请输入标签'}
                }
            }
        }
    })
    .on('success.form.bv', function(e) {
        e.preventDefault();
        added_channel = copy_obj(model.channel.$model);
        console.log("added_channel: ", added_channel);
        $.post("/channel/add",
               model.channel.$model,
               function(data) {
                   var result = data.result;
                   if (result == "success") {
                       msg_sth("成功添加一个渠道: " + added_channel.name);
                       handle_cancel_add();
                       model.added_channels.unshift(added_channel);
                       model.added_channels_amount = model.added_channels_amount + 1;
                   }
                   else {
                       vex_alert_error("请检查新加渠道的字段是否和已有的重复");
                   };
                   $('button[data-loading-text]').button('reset');
               }
              );
    })
    .on('error.form.bv', function(e) {
        e.preventDefault();
        $('button[data-loading-text]').button('reset');
        console.log("error");
    });
});
