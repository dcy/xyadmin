$(document).ready(function() {
    var model = avalon.define({
        $id: "channel_model",
        channels: [],
        channels_amount: 0,
        to_add: function() {
            window.location.href = "/channel/add"
        },
        del_channel: function(channel, remove_fun) {
            vex_confirm("确定删除 " + channel.name + " 渠道吗?", function(value) {
                if (value) {
                    $.post("/channel/del",
                           {id: channel.id},
                           function(data) {
                               if (data.result == "success") {
                                   msg_sth("成功删除渠道 " + channel.name);
                                   remove_fun();
                                   refresh_channels_amount();
                               }
                               else {
                                   var serverlist = data.serverlist;
                                   var what = "服务器列表'"+serverlist.svr_id+":"+serverlist.name+"'配置了该渠道";
                                   vex_alert_error(what);
                               }
                           });
                };
            });
        }
    });

    function refresh_channels_amount() {
        model.channels_amount = (model.channels.length)
    };

    get_channels();

    function get_channels() {
        $.get("/channel/get_channels",
              {unixtime: unixtime()},
              function(data) {
                  model.channels = sort_by_key_asc(data.channels, "id");
                  refresh_channels_amount();
              }
             )
    };
});
