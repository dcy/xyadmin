$(document).ready(function() {
    var model = avalon.define({
        $id: "sysmsg_model",
        sysmsgs: [],
        sysmsgs_amount: 0,
        to_send: function() {
            window.location.href = "/sysmsg/send"
        },
        del_sysmsg: function(sysmsg, remove_fun) {
            vex_confirm("确定删除该条系统消息记录吗？", function(value) {
                if (value) {
                    $.post("/sysmsg/del",
                           {id: sysmsg.id},
                           function(data) {
                               if (data.result == "success") {
                                   msg_sth("成功删除系统消息记录");
                                   remove_fun();
                                   refresh_sysmsgs_amount();
                               }
                           }
                          )
                }
            })
        }
    });

    function refresh_sysmsgs_amount() {
        model.sysmsgs_amount = model.sysmsgs.length
    };

    get_sysmsgs();

    function get_sysmsgs() {
        $.get("/sysmsg/get_sysmsgs",
               {unixtime: unixtime()},
               function(data) {
                   model.sysmsgs = sort_by_key_desc(data.sysmsgs, "id");
                   refresh_sysmsgs_amount();
               })
    };


});
