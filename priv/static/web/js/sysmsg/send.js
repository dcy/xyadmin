$(document).ready(function() {
    //$('#timePicker')
    //.datetimepicker({
    //    format: "yyyy-mm-dd hh:ii:00",
    //    language: "zh-CN",
    //    todayBtn: true,
    //    pickerPosition: "bottom-left",
    //    autoclose: "true"
    //})
    //.on('changeDate', function(e) {
    //    $('#send_sysmsg').data('bootstrapValidator').updateStatus('start_time', 'NOT_VALIDATED', null).validateField('start_time');

    //});
    $('#datetimepicker2').datetimepicker({
        format: "YYYY-MM-DD HH:mm:00"
    });

    var model = avalon.define({
        $id: "send_sysmsg",
        types: G_SYSMSG_TYPES,
        sysmsg: {type:"", start_time: "", times: "", interval_minute: "", serverlists: [], content: ""},
        cancel: handle_cancel_send,
        to_list: function() {
            window.location.href = "/sysmsg/list"
        },
        serverlists: []
    });

    get_serverlists();
    function get_serverlists() {
        $.get("/serverlist/get_serverlists",
              {unixtime: unixtime()},
              function(data) {
                  model.serverlists = data.serverlists
              })
    };
    $(".basic-multiple").select2();

    function handle_cancel_send() {
        model.sysmsg.type = "";
        model.sysmsg.start_time = "";
        model.sysmsg.times = "";
        model.sysmsg.interval_minute = "";
        //model.sysmsg.serverlists = [];
        $('.basic-multiple').val([]).trigger("change");
        model.sysmsg.content = "";
        $('#send_sysmsg').bootstrapValidator('revalidateField', 'type')
        .bootstrapValidator('revalidateField', 'start_time')
        .bootstrapValidator('revalidateField', 'times')
        .bootstrapValidator('revalidateField', 'interval_minute')
        .bootstrapValidator('revalidateField', 'serverlists')
        .bootstrapValidator('revalidateField', 'content');

    };

    avalon.ready(function() {
        $('#send_sysmsg').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                type: {
                    validators: {
                        notEmpty: {message: '请选择公告类型'}
                    }
                },
                start_time: {
                    validators: {
                        notEmpty: {message: '请输入开始时间'}
                    }
                },
                times: {
                    validators: {
                        stringLength: {min:1, max:2},
                        notEmpty: {message: '请输入循环次数'}
                    }
                },
                interval_minute: {
                    validators: {
                        stringLength: {min:1, max:3},
                        notEmpty: {message: '请输入循环间隔'}
                    }
                },
                serverlists: {
                    validators: {
                        notEmpty: {message: '请选择服务器列表'}
                    }
                },
                content: {
                    validators: {
                        stringLength: {min:1, max:160},
                        notEmpty: {message: '请输入内容'}
                    }
                }
            }

        })
        .on('error.form.bv', function(e) {
            $('button[data-loading-text]').button('reset');
        })
        .on('success.form.bv', function(e) {
            e.preventDefault();
            var sysmsg_vals = model.sysmsg.$model;
            sysmsg_vals.serverlists = $('#serverlists').val();
            sysmsg_vals.start_time = $('#start_time').val();
            $.post("/sysmsg/send",
                   sysmsg_vals,
                   function(data) {
                       var svrs = data.fail_svrs
                       var msg = ""
                       if (svrs.length == 0) {
                           msg = "所选服务器全部发送成功"
                       }
                       else {
                           msg = "以下服务器失败: "
                           for (x in svrs) {
                               var svr = svrs[x]
                               msg = msg + "[" + svr.svr_id + ":" + svr.name + "]"
                           }
                       }
                       vex_alert_error(msg)
                       reset_button()
                   })
        })

    });


});
