$(document).ready(function() {

    G_SVR_STATES = [
        {"value": 1, "name": "维护中"},
        {"value": 2, "name": "普通"},
        {"value": 3, "name": "爆满"},
        {"value": 4, "name": "新服"}
    ];

    //前后端都有类似一份配置
    G_SVR_TYPES = [
        {"value": 1, "name": "QA环境"},
        {"value": 2, "name": "审核环境"},
        {"value": 3, "name": "正式环境"}
    ];

    G_SYSMSG_TYPES = [
        {"value": 0, "name": "滚动消息"},
        {"value": 1, "name": "普通消息"}
    ];

    G_PLATFORMS = [
        {"value": 1, "name": "Android"},
        {"value": 2, "name": "IOS"}
    ];

    G_ITEM_TYPES = [
        {type_id: 0, name: "请选择物品类型", need_item_id: false},
        {type_id: 1, name: "武将", need_item_id: true, conf:"General.csv"},
        {type_id: 2, name: "装备", need_item_id: true, conf:"Equipment.csv"},
        {type_id: 3, name: "武将碎片", need_item_id: true, conf:"General.csv"},
        {type_id: 5, name: "矿物", need_item_id: false},
        {type_id: 6, name: "元宝", need_item_id: false},
        {type_id: 7, name: "体力", need_item_id: false},
        {type_id: 8, name: "金币", need_item_id: false},
        {type_id: 9, name: "木材", need_item_id: false},
        //{type_id: 12, name: "喇叭", need_item_id: false},
        {type_id: 18, name: "卷轴", need_item_id: true, conf:"Scroll.csv"},
        {type_id: 19, name: "悬赏令", need_item_id: false},
        {type_id: 20, name: "竞技币", need_item_id: false},
        {type_id: 21, name: "士兵进阶丹", need_item_id: true, conf:"soldierToken.csv"},
        {type_id: 23, name: "武将经验丹", need_item_id: true, conf:"GeneralExpPill.csv"},
        {type_id: 27, name: "武将官职材料", need_item_id: true, conf:"Material.csv"},
        {type_id: 29, name: "扫荡券", need_item_id:false}
    ];

    G_FUNCTIONS = [
        {func_id: 1, name: "签到"},
        {func_id: 2, name: "目标任务"},
        {func_id: 3, name: "任务"},
        {func_id: 4, name: "武将"},
        {func_id: 5, name: "好友"},
        {func_id: 6, name: "聊天"},
        {func_id: 7, name: "士兵"},
        {func_id: 8, name: "装备"},
        {func_id: 9, name: "征战"},
        {func_id: 10, name: "副本"},
        {func_id: 11, name: "购买军令"},
        {func_id: 12, name: "炼金"},
        {func_id: 13, name: "充值"},
        {func_id: 14, name: "邮件"},
        {func_id: 15, name: "封地"},
        {func_id: 16, name: "竞技场"},
        {func_id: 17, name: "点将台"},
        {func_id: 18, name: "公会"},
        {func_id: 19, name: "排行榜"}
    ];


    avalon.config({
        interpolate:["[[","]]"]
    });
    Vue.config.delimiters = ['[[', ']]'];
    vex.dialog.buttons.YES.text = '确定';
    vex.dialog.buttons.NO.text = '取消';
    if (typeof yxq_conf != "undefined") {
        g_is_login = yxq_conf.is_login;
    };

    $('input[maxlength]').maxlength();
    $('textarea[maxlength]').maxlength();

    $('.bt-switch').bootstrapSwitch({
        onText: "是",
        offText: "否"
    });

    $('input.icheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

    $('button[data-loading-text]').click(function () {
        var btn = $(this).button('loading');
        //setTimeout(function () {
        //    btn.button('reset');
        //}, 3000);
    });

    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'future'
    };

    var datas = get_url_datas();
    var content;
    if (datas.content) {
        content = decodeURI(datas.content);
    }
    else {
        content = "";
    };
});

handle_svr_resp = function(data){
    result = data.result;
    if (result == "success"){
        location.reload();
    }
    else if (result == "not_login"){
        location.href = "/account/login";
    }
    else if (result =="game_not_found"){
        location.href = "/pages/not_found";
    }
};

function get_url_datas()
{
    var aQuery = window.location.href.split("?");  //取得Get参数
    var aGET = new Object();
    if(aQuery.length > 1) {
        var aBuf = aQuery[1].split("&");
        for(var i=0, iLoop = aBuf.length; i<iLoop; i++) {
            var aTmp = aBuf[i].split("=");  //分离key与Value
            aGET[aTmp[0]] = aTmp[1];
        }
    }
    return aGET;
}

function redirect_404()
{
    location.href = "/pages/not_found";
}

function msg_sth(something)
{
    Messenger().post({
        message: something,
        showCloseButton: true
    });
}

function vex_alert_error(what) {
    var message = '<div class="text-center text-danger">' + what + '</div>';
    vex.dialog.alert({message: message});
}

function vex_sth(What, callback) {
    var message = '<div class="text-center">' + What + '</div>';
    vex.open({
        content: message,
        showCloseButton: true,
        afterClose: callback
    });
}

function vex_confirm(what, callback) {
    var message = '<div class="text-center text-danger">' + what + '</div>';
    vex.dialog.confirm({
        message: message,
        callback: callback
    });
}

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
};

// Generate a sum of two random numbers
function generateCaptcha() {
    $('#captchaOperation')
    .html([randomNumber(1, 100), '+', randomNumber(1, 100), '='].join(' '));
};

function unixtime() {
    return new Date().getTime();
};

function handle_general_error(result) {
    if (result == "not_login"){
        location.href = "/account/login";
        return
    }
    else if (result =="not_found"){
        vex_alert_error("你所要访问的东西不存在");
        return
    }
};

function copy_obj(obj) {
    var new_obj = {};
    var i;
    for (i in obj) {
        new_obj[i] = obj[i];
    };
    return new_obj;
};

function sort_by_key_asc(objs, key) {
    function sort_key(f_obj, b_obj) {
        return f_obj[key] - b_obj[key];
    };
    return objs.sort(sort_key);
}; 

function sort_by_key_desc(objs, key) {
    function sort_key(f_obj, b_obj) {
        return b_obj[key] - f_obj[key];
    };
    return objs.sort(sort_key);
};

function remove_array_element(items, item) {
    while (items.indexOf(item) !== -1) {
        items.splice(items.indexOf(item), 1);
    };
    return items;
}

function get_item_type(type_id) {
    for (x in G_ITEM_TYPES) {
        var item_type = G_ITEM_TYPES[x];
        if (item_type.type_id == type_id) {
            return item_type
        }
    }
    return null
}

function get_locked_funcs(func_id) {
    for (x in G_FUNCTIONS) {
        var func = G_FUNCTIONS[x]
        if (func.func_id == func_id) {
            return func
        }
    }
    return {func_id: func_id, name: "not_exist"}
}

function get_by_key(items, key_name, key_value) {
    for (x in items) {
        var item = items[x]
        if (item[key_name] == key_value) {
            return item
        } 
    }
    return null
}

function reset_button() {
    $('button[data-loading-text]').button('reset')
}

function load_serverlists() {
    $.get("/serverlist/get_serverlists",
          {unixtime: unixtime()},
          function (data) {
              var serverlists = data.serverlists
              return serverlists
              console.log("data", data)
              return data.serverlists
          })
}
