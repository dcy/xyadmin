-module(xyadmin_init).
-export([init/0, stop/1]).
-include("include/xyadmin.hrl").

init() ->
    application:start(asn1),
    application:start(public_key),
    application:start(ssl),
    qdate:start(),
    ?TRACE_VAR(xyadmin_init),
    {ok, _Sup}= xyadmin_app:start(),
    {ok, []}.

stop([]) ->
    ?TRACE_VAR(xyadmin_init_stop),
    ok.
